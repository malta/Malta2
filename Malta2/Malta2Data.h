#ifndef MALTA2DATA_H
#define MALTA2DATA_H

/**********************
 * Class Malta2Data
 * Abhishek.Sharma@cern.ch
 * Carlos.Solans@cern.ch
 * January 2018
 **********************/

#include <vector>
#include <cstdint>
#include <string>

/**
 * A MALTA2 word is composed of 40 bits, and can contain up to 16 hits, 
 * which are read-out from the FPGA 
 * by the consecutive reading of two 32-bit words with MaltaBase::ReadMaltaWord.
 * These readings are identified as \c word1 and \c word2, and are decoded by Malta2Data.
 * Malta2Data requires an object to be created, and the value of \c word1, and 
 * \c word2 to be passed to Malta2Data::setWord1 and Malta2Data::setWord2.
 * The contents of the words then decoded by Malta2Data::unpack, and the meaning
 * of the bits stored in internal memories that are accessible through dedicated
 * methods: Malta2Data::getRefbit, Malta2Data::getPixel, Malta2Data::getGroup, 
 * Malta2Data::getParity, Malta2Data::getDelay, Malta2Data::getDcolumn, Malta2Data::getBcid,
 * Malta2Data::getChipbcid, Malta2Data::getChipid, Malta2Data::getPhase, Malta2Data::getWinid,
 * Malta2Data::getL1id. 
 * 
 * Additionally, the row and column values of each hit encoded in a MALTA2 word, 
 * are available through the Malta2Data::getHitRow and Malta2Data::getHitColumn.
 * The number of avilable hits in the MALTA2 word is Malta2Data::getNhits.
 *
 * It is possible to encode the MALTA2 word as two 32-bit words for testing purposes.
 * This is done by setting the value of the bits with dedicated methods (
 * Malta2Data::setRefbit, Malta2Data::setPixel, Malta2Data::setGroup, 
 * Malta2Data::setParity, Malta2Data::setDelay, Malta2Data::setDcolumn, Malta2Data::setBcid,
 * Malta2Data::setChipbcid, Malta2Data::setChipid, Malta2Data::setPhase, Malta2Data::setWinid,
 * Malta2Data::setL1id)
 * and then calling Malta2Data::pack that makes the words available through 
 * Malta2Data::getWord1 and Malta2Data::getWord2.
 * 
 *
 * @brief Tool encode and decode MALTA2 hit words.
 * @author Carlos.Solans@cern.ch
 * @author Abhishek.Sharma@cern.ch
 * @date January 2018
 **/

class Malta2Data{

 public:
  
  /**
   * @brief Initialize internal arrays
   **/
  Malta2Data();
  
  /**
   * @brief Delete internal arrays
   **/
  ~Malta2Data();

  /**
   * @brief Set pixel hit coordinates (col, row)
   * @param col the x-coordinate of the hit
   * @param row the y-coordinate of the hit
   **/
  void setHit(uint32_t col, uint32_t row);

  /**
   * @brief Set Reference bit
   * @param value the reference bit
   **/
  void setRefbit(uint32_t value);

  /**
   * @brief Set pixel hit pattern (16 bits)
   * @param value the pixel hit pattern
   **/
  void setPixel(uint32_t value);

  /**
   * @brief Set group
   * @param value the group
   **/
  void setGroup(uint32_t value);

  /**
   * @brief Set parity bit
   * @param value the parity bit
   **/
  void setParity(uint32_t value);

  /**
   * @brief Set delay
   * @param value the delay
   **/
  void setDelay(uint32_t value);

  /**
   * @brief Set double column 
   * @param value the double column
   **/
  void setDcolumn(uint32_t value);

  /**
   * @brief Set chip ID
   * @param value the chip ID
   **/
  void setChipid(uint32_t value);

  /**
   * @brief Set chip BCID
   * @param value the chip BCID
   **/
  void setChipbcid(uint32_t value);

  /**
   * @brief Set phase
   * @param value the phase
   **/
  void setPhase(uint32_t value);

  /**
   * @brief Set Window ID
   * @param value the window ID
   **/
  void setWinid(uint32_t value);

  /**
   * @brief Set BCID
   * @param value the BCID to set
   **/
  void setBcid(uint32_t value);

  /**
   * @brief Set L1ID
   * @param value the L1ID to set 
   **/
  void setL1id(uint32_t value);

  /**
   * @brief Set word1 as a uint32_t and use only 31 lower bits.
   * @param value the 1st word
   **/
  void setWord1(uint32_t value);

  /**
   * @brief Set word2 as a uint32_t and use only 31 lower bits.
   * @param value the 2nd word
   **/
  void setWord2(uint32_t value);

  /**
   * @brief Get Reference bit
   * @return uint32_t Reference bit
   **/
  uint32_t getRefbit();

  /**
   * @brief Get pixel group
   * @return uint32_t pixel group
   **/
  uint32_t getPixel();

  /**
   * @brief Get group
   * @return uint32_t group
   **/
  uint32_t getGroup();

  /**
   * @brief Get parity
   * @return uint32_t parity
   **/
  uint32_t getParity();

  /**
   * @brief Get delay
   * @return uint32_t delay
   **/
  uint32_t getDelay();

  /**
   * @brief Get double column
   * @return uint32_t double column
   **/
  uint32_t getDcolumn();

  /**
   * @brief Get BCID
   * @return uint32_t BCID
   **/
  uint32_t getBcid();

  /**
   * @brief Get Chip BCID
   * @return uint32_t Chip BCID
   **/
  uint32_t getChipbcid();

  /**
   * @brief Get chip ID
   * @return uint32_t chip ID
   **/
  uint32_t getChipid();
  
  /**
   * @brief Get phase
   * @return uint32_t phase
   **/
  uint32_t getPhase();
  
  /**
   * @brief Get Win ID
   * @return uint32_t win ID
   **/
  uint32_t getWinid();
  
  /**
   * @brief Get L1ID
   * @return uint32_t L1ID
   **/
  uint32_t getL1id();

  /**
   * @brief Get word1
   * @return uint32_t word
   **/
  uint32_t getWord1();

  /**
   * @brief Get word2
   * @return uint32_t word
   **/
  uint32_t getWord2();

  /**
   * @brief Get number of hits
   * @return uint32_t number of hits
   **/
  uint32_t getNhits();

  /**
   * @brief Get row for given hit
   * @param hit hit number
   * @return uint32_t row of the hit
   **/
  uint32_t getHitRow(uint32_t hit);

  /**
   * @brief Get column for given hit
   * @param hit hit number
   * @return uint32_t column of the hit
   **/
  uint32_t getHitColumn(uint32_t hit);

  /**
   * @brief Get String representation
   * @return string of bits
   **/
  std::string toString();

  /**
   * @brief Get info
   * @return string 
   **/
  std::string getInfo();
    
  /**
   * @brief Encode the bits into a word
   **/
  void pack();

  /**
   * @brief Decode the word into bits
   **/
  void unPack();

  /**
   * @brief Decode the word into bits
   **/
  void unpack();

  /**
   * @brief Dump the data to the screen
   **/
  void dump();
  
 private:
  uint32_t m_refbit;
  uint32_t m_pixel;
  uint32_t m_group;
  uint32_t m_parity;
  uint32_t m_delay;
  uint32_t m_dcolumn;
  uint32_t m_chipbcid;
  uint32_t m_chipid;
  uint32_t m_phase;
  uint32_t m_winid;
  uint32_t m_bcid;
  uint32_t m_l1id;
  uint32_t m_nhits;
  uint32_t m_word1;
  uint32_t m_word2;
  std::vector<uint32_t> m_rows;
  std::vector<uint32_t> m_columns;
  //std::vector<bool> m_data;

  //uint32_t lst2int(uint32_t pos1, uint32_t pos2);
  //void int2lst(uint32_t value, uint32_t pos1, uint32_t pos2);

};

#endif
