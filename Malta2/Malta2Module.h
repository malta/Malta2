#ifndef MALTA2MODULE_H
#define MALTA2MODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2.h"

#include <string>
#include <thread>
#include <map>

#include "TCanvas.h"

/**
 * Malta2Module is an implementation of a ReadoutModule for 
 * the test-beam data-taking application (MaltaMultiDAQ)
 * to control MALTA2.
 * Malta2Module can be loaded dynamically thrhough the ModuleLoader 
 * as a ReadoutModule object.
 *
 * @verbatim

   ReadoutModule * malta2 = ModuleLoader::getInstance()->newReadoutModule("Malta2","Malta2Module",
                                                                          "PLANE_1","192.168.200.10","");
																		 
   @endverbatim
 * 
 * Malta2Module connects to the MALTA FPGA on creation, and configures it
 * for operation with Malta2Module::Configure. 
 * The configuration is steered from the ReadoutConfig object that is 
 * loaded from a configuration text file through ConfigParser.
 * 
 * @verbatim

   [PLANE 1]
   type: MALTA2
   wafer: W4R10
   address: 192.168.200.10
   CONFIGURATION_VOLTAGE: 0.8
   SC_ENABLE_MON_DAC_CURRENT: false
   SC_ENABLE_MON_DAC_VOLTAGE: false
   SC_IDB: 100
   SC_ITHR: 30
   SC_IRESET: 10
   SC_ICASN: 10
   SC_VCASN: 64
   SC_VCLIP: 127
   SC_VPULSE_HIGH: 127
   SC_VPULSE_LOW: 127
   SC_VRESET_P: 45
   SC_VRESET_D: 64
   @endverbatim
 *
 * Malta2Module fills 3 histograms, the distribution of the 
 * time of the hits (ReadoutModule::GetTimingHisto),
 * the number of hits per event (ReadoutModule::GetNHitHisto),
 * the hit map (ReadoutModule::GetHitMapHisto).

 * The methods implemented in this class are the following:
 * * Malta2Module::SetInternalTrigger configures the plane for internal triggering.
 * * Malta2Module::Configure configures the plane for data taking. 
 * * Malta2Module::Start starts the data taking on the plane by spawning the Run method.
 * * Malta2Module::Stop stops the data taking on the plane by stopping the Run thread.
 * * Malta2Module::EnableFastSignal enables the trigger signal out from the plane if any.
 * * Malta2Module::DisableFastSignal disables the trigger signal out from the plane if any.
 *
 *
 * @brief MALTA2 ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @date March 2021
 **/

class Malta2Module: public ReadoutModule{
  
 public:
  
  /**
   * @brief: create a malta2 module
   * @param name the module name
   * @param address the ipbus connection string
   * @param outdir the output directory
   **/
  Malta2Module(std::string name, std::string address, std::string outdir);

  /**
   * @brief: delete pointers
   **/
  ~Malta2Module();
  
  /**
   * @brief Set the module for internal triggering
   **/
  void SetInternalTrigger();

  /**
   * @brief Configure MALTA2 for data taking
   **/
  void Configure();
  
  /**
   * @brief Start data acquisition thread
   * @param run the run number as a string
   * @param usePath use the path 
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * Brief: Stop the data acquisition thread
   **/
  void Stop();

  /**
   * @brief The data acquisition thread
   **/
  void Run();

  /**
   * @brief get the L1A from the FPGA
   * @return L1A 
   **/
  uint32_t GetNTriggers();

  /**
   * @brief enable fast signal
   **/
  void EnableFastSignal();

  /**
   * @brief: disable fast signal
   **/
  void DisableFastSignal();

 private:  
  
  Malta2 * m_malta2;
  Malta2Tree * m_ntuple;
  bool m_internalTrigger;
  TH1D* m_maskDColHist;
  TH2I* m_maskedPixelHist;
  TH2I* m_maskedPixelGhostsHist;

};

#endif

