#ifndef MALTA2MASKER
#define MALTA2MASKER

#include <cstdint>
#include <vector>
#include <utility>

/**
 * This class is a tool to help define the mask steps for a scan
 *
 * @verbatim
 Masker m;
 m.SetRange(0,0,512,512);
 m.SetShape(8,2);
 m.SetFrequency(1);
 m.Build();
 for(int st=0; st<m.GetNumSteps(); st++){
   m.GetStep(st);
   m.GetPixels();
   m.GetDoubleColumns();
 }  
	 
 @endverbatim
 * 
 *
 * @author Carlos.Solans@cern.ch
 * @date February 2024
 **/

class Malta2Masker{
 public:
  
  /**
   * Default constructor. Create an empty Masker.
   **/
  Malta2Masker();
  
  /**
   * Default destructor. Delete all memory contents.
   **/
  ~Malta2Masker();
  
  /**
   * Enable/Disable the verbose mode
   * @param verbose Enable verbose if true
   */
  void SetVerbose(bool verbose);
  
  /**
   * Set the range of the scan as a rectangle 
   * from (px0,py0) to (pxN,pyN)
   * @param px0 X coordinate (column) of the starting pixel
   * @param py0 Y coordinate (row) of the starting pixel
   * @param pxN X coordinate (column) of the ending pixel
   * @param pyN Y coordinate (row) of the ending pixel
   **/
  void SetRange(uint32_t px0, uint32_t py0, uint32_t pxN, uint32_t pyN);
  
  /**
   * Set the size of the enabled pixels
   * @param size_x size of the shape in X (columns)
   * @param size_y size of the shape in Y (columns)
   **/
  void SetShape(uint32_t size_x, uint32_t size_y);
  
  /**
   * Set the number of times the shape should be included in the range.
   * The larger the frequency, the fewer the number of total steps 
   * to cover the whole range.
   * @param freq The repetition frequency of the shape in the range.
   **/
  void SetFrequency(uint32_t freq);
  
  /**
   * Calculate the number of steps needed to scan the range
     **/
  void Build();

  /**
   * Get the total number of steps needed to scan the range.
   * This needs to be called after Masker::Build
   * @return the number of steps
   **/
  uint32_t GetNumSteps();
  
  /**
   * Get the given step. 
   * This needs to be called after Masker::Build
   * @param step Step to get. Cannot exceed Masker::GetNumSteps
   **/
  void GetStep(uint32_t step);
  
  /**
   * Helper method to get the next step without knowing the actual step sequence.
   * This needs to be called after Masker::Build
   **/
  void GetNextStep();
  
  /**
   * Get the double columns that are active in this step
   * @return vector of double columns
   **/
  std::vector<uint32_t> & GetDoubleColumns();

  /**
   * Get the pixels that are active in this step
   * @return vector of pixels described as pairs (col, row)
   **/
  std::vector<std::pair<uint32_t,uint32_t> > & GetPixels();
  
  /**
   * Get the Aura pixels that are going to be injected in this step
   * @return vector of pixels described as pairs (col, row)
   **/
  std::vector<std::pair<uint32_t,uint32_t> > & GetAuraPixels();
  
  /**
   * Check if the given column, row is expected in the current mask step
   * @param column the hit column to compare
   * @param row the hit row to compare
   */
  bool Contains(uint32_t column, uint32_t row);
    
private:
  
  uint32_t m_num_steps;
  uint32_t m_num_steps_x;
  uint32_t m_num_steps_y;
  uint32_t m_freq;
  uint32_t m_cur_step;
  uint32_t m_px0;
  uint32_t m_pxN;
  uint32_t m_py0;
  uint32_t m_pyN;
  uint32_t m_gap;
  uint32_t m_size_x;
  uint32_t m_size_y;
  bool m_verbose;
  bool m_doAura;
  
  std::vector<uint32_t> m_double_cols;
  std::vector<std::pair<uint32_t,uint32_t> > m_pixels;
  std::vector<std::pair<uint32_t,uint32_t> > m_auraPixels;

};


#endif
