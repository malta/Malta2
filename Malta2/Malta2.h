#ifndef MALTA2_H
#define MALTA2_H

#include "ipbus/Uhal.h"
#include <map>
#include <vector>

class TH2I;
class TH1D;

/**
 * Malta2 contains all the necessary methods to control the 
 * MALTA2 Pixel detector prototype through ipbus.
 *
 * @verbatim    

   Malta2 * malta2 = new Malta2();
   malta2->Connect("udp://192.168.200.10");
   
   malta2->EnableMerger(false,true);
   malta2->SetPulseWidth(750,true);

   malta2->EnableMonDacCurrent(false,true);
   malta2->EnableMonDacVoltage(true,true);
   malta2->EnableIDB(false,true);
   malta2->EnableITHR(false,true);
   malta2->EnableIRESET(false,true);
   malta2->EnableICASN(false,true);
   malta2->EnableVCASN(false,true);
   malta2->EnableVCLIP(false,true);
   malta2->EnableVPULSE_HIGH(false,true);
   malta2->EnableVPULSE_LOW(false,true);
   malta2->EnableVRESET_P(false,true);
   malta2->EnableVRESET_D(false,true);

   @endverbatim
 *

 * 
 * @brief MALTA2 control and read-out class
 * @author Ignacio.Asensi@cern.ch
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 * @date March 2021 - Added missing registers 
 **/
class Malta2{
 public:

  static const uint32_t VERSION_CHECK=18;

  static const uint32_t SC_REG0=17; //begining of registers for SC 
  static const uint32_t SC_REGN=61;
  static const uint32_t SC_NREGS=76;

  static const uint32_t SIMPLE_WORDS_0=17;
  static const uint32_t SIMPLE_WORDS_N=61;
  
  static const uint32_t FIFO_WORDS_0=0;
  static const uint32_t FIFO_WORDS_N=16;
  static const uint32_t NTAPS             = 40;   // number of taps 
  
 
  static const uint32_t IPBADDR_VERSION   = 0;   // ipbus register, version number
  static const uint32_t IPBADDR_STATUS    = 1;   // ipbus register, RO constant number 
  static const uint32_t IPBADDR_RESET     = 2;   // ipbus register, reset
  static const uint32_t IPBADDR_BUSY      = 3;   //!< ipbus register, busy info
  static const uint32_t IPBADDR_AOFIFO    = 6;   //!< ipbus address, AO FIFO (data)
  static const uint32_t IPBADDR_DUMCTR    = 8;   //!< Control register 
  static const uint32_t IPBADDR_DELAY     = 9;   //!< Pipeline depth control and fast signal enable
  static const uint32_t IPBADDR_TAP0      = 0xA; //!< ipbus address, first address to write tap  
  static const uint32_t IPBADDR_TAP0_R    = 0x32;//!< ipbus address, first address to read tap
  static const uint32_t IPBADDR_WDCTR     = 91;  //!< ipbus address, number of words in AO FIFO (data)  
  static const uint32_t AO_ADDR_RODELAY   = 92;
  
  static const uint32_t IPBADDR_SCCONTROL = 155; //!< ipbus register, control of slow control
  static const uint32_t IPBADDR_MULT_1_W  = 114; //!<< Write slow control multi-register 1
  static const uint32_t IPBADDR_MULT_2_W  = 115; //!< Write slow control multi-register 2
  static const uint32_t IPBADDR_MULT_3_W  = 116; //!< Write slow control multi-register 3
  static const uint32_t IPBADDR_MULT_4_W  = 117; //!< Write slow control multi-register 4
  static const uint32_t IPBADDR_MULT_1_R  = 118; //!< Read slow control multi-register 1
  static const uint32_t IPBADDR_MULT_2_R  = 119; //!< Read slow control multi-register 2
  static const uint32_t IPBADDR_MULT_3_R  = 120; //!< Read slow control multi-register 3
  static const uint32_t IPBADDR_MULT_4_R  = 121; //!< Read slow control multi-register 4
  static const uint32_t IPBADDR_FIFO_W    = 183; //!< Write slow control FIFO register
  static const uint32_t IPBADDR_FIFO_R    = 184; //!< Read slow control FIFO register

  static const uint32_t FIFO1_FULL    = 0x00000001; //!< Busy register, Data FIFO 1 mask for full
  static const uint32_t FIFO2_FULL    = 0x00000002; //!< Busy register, Data FIFO 2 mask for full
  static const uint32_t FIFO1_EMPTY   = 0x00000008; //!< Busy register, Data FIFO 1 mask for empty
  static const uint32_t FIFO2_EMPTY   = 0x00000010; //!< Busy register, Data FIFO 2 mask for empty
  static const uint32_t FIFO1_HALF    = 0x00000040; //!< Busy register, Data FIFO 1 mask for half-full
  static const uint32_t FIFO2_HALF    = 0x00000080; //!< Busy register, Data FIFO 2 mask for half-full
  static const uint32_t L1A_COUNTER   = 0xfff00000; //!< Busy register, L1ID 
  static const uint32_t FIFOM_EMPTY   = 0x000020; 

  static const uint32_t CTRL_AORESET  = 0x00000004; //!< Control register, rese
  static const uint32_t CTRL_INCTRIG  = 0x00080000; //!< Control register, increase the trigger
  static const uint32_t CTRL_L1A_RST  = 0x00010000; //!< Control register, reset the L1A counter
  static const uint32_t CTRL_L1A_EXT  = 0x00040000; //!< Control register, enable external L1A
  static const uint32_t CTRL_PULSE    = 0x00100000; //!< Control register, pulse
  static const uint32_t CTRL_RO_OFF   = 0x20000000; //!< Control register, disable the read-out
  static const uint32_t CTRL_HALFCOLS = 0x40000000; //!< Control register, disable half columns in the read-out
  static const uint32_t CTRL_HALFROWS = 0x80000000; //!< Control register, disable half rows in the read-out
  
  static const uint32_t DELAY_FAST_EN = 0x00000040; //!< Delay register, enable fast signal
    
  
  enum SLOWCONTROL {
    
    //128
    SET_VCASN       =  0, //!< VCASN register, 128 bits, 1 hot encoded
    SET_VCLIP       =  1, //!< VCLIP register, 128 bits, 1 hot encoded
    SET_VPULSE_HIGH =  2, //!< VPULSE_HIGH register, 128 bits, 1 hot encoded
    SET_VPULSE_LOW  =  3, //!< VPULSE_LOW register, 128 bits, 1 hot encoded
    SET_VRESET_P    =  4, //!< VRESET_P register, 128 bits, 1 hot encoded
    SET_VRESET_D    =  5, //!< VRESET_D register, 128 bits, 1 hot encoded
    SET_ICASN       =  6, //!< ICASN register, 128 bits, thermometer encoded
    SET_IRESET      =  7, //!< IRESET register, 128 bits, thermometer encoded
    SET_ITHR        =  8, //!< ITHR register, 128 bits, thermometer encoded
    SET_IBIAS       =  9, //!< IBIAS register, 128 bits, thermometer encoded
    SET_IDB         = 10, //!< IDB register, 128 bits, thermometer encoded
    //256 
    MASK_FULLCOL           = 14,
    //512 (treated as 256x2)
    MASK_COL               = 11,
    MASK_HOR               = 12,
    MASK_DIAG              = 13,
    PULSE_COL              = 15,
    PULSE_HOR              = 16,  

    //individual registers
    DATAFLOW_MERGERTOLEFT  = 17, //!< Dataflow register, 1-bit, send merger data to the left
    DATAFLOW_MERGERTORIGHT = 18, //!< Dataflow register, 1-bit, send merger data to the right
    DATAFLOW_LMERGERTOLVDS = 19, //!< Dataflow register, 1-bit, send merger data to LVDS and to the left CMOS -- this makes no sense
    DATAFLOW_LMERGERTOCMOS = 20, //!< Dataflow register, 1-bit, send left merger data to CMOS -- this makes no sense
    DATAFLOW_RMERGERTOLVDS = 21, //!< Dataflow register, 1-bit, send merger data to LVDS and to the right CMOS -- this makes no sense
    DATAFLOW_RMERGERTOCMOS = 22, //!< Dataflow register, 1-bit, send right merger data to CMOS -- this makes no sense
    DATAFLOW_LCMOS         = 23, //!< Dataflow register, 1-bit, receive data from left CMOS 
    DATAFLOW_RCMOS         = 24, //!< Dataflow register, 1-bit, receive data from right CMOS
    DATAFLOW_ENLVDS        = 25, //!< Dataflow register, 1-bit, enable the LVDS
    DATAFLOW_ENLCMOS       = 26, //!< Dataflow register, 1-bit, enable the left CMOS
    DATAFLOW_ENRCMOS       = 27, //!< Dataflow register, 1-bit, enable the right CMOS
    DATAFLOW_ENMERGER      = 28, //!< Dataflow register, 1-bit, enable the merger
    POWER_SWITCH_LEFT      = 29, //!< Power register, 1-bit, connect DAC DVDD and matrix DVDD on the left of the chip
    POWER_SWITCH_RIGHT     = 30, //!< Power register, 1-bit, connect DAC DVDD and matrix DVDD on the right of the chip
    LVDS_EN                = 31, //!< LVDS register, 1-bit, enable the LDVS
    LVDS_PRE               = 32, //!< LVDS register, 1-bit, enable the pre-emphasis
    LVDS_BRIDGE_EN         = 33, //!< LVDS register, 1-bit, enable the LDVS
    LVDS_CMFB_EN           = 34,
    LVDS_SET_IBCMFB        = 35,
    LVDS_SET_IVPH          = 36,
    LVDS_SET_IVPL          = 37,
    LVDS_SET_IVNH          = 38,
    LVDS_SET_IVNL          = 39,
    SWCNTL_VCASN           = 40,
    SWCNTL_VCLIP           = 41,
    SWCNTL_VPULSE_HIGH     = 42,
    SWCNTL_VPULSE_LOW      = 43,
    SWCNTL_VRESET_P        = 44,
    SWCNTL_VRESET_D        = 45,
    SWCNTL_ICASN           = 46,
    SWCNTL_IRESET          = 47,
    SWCNTL_IBIAS           = 48,
    SWCNTL_ITHR            = 49,
    SWCNTL_IDB             = 50,
    SWCNTL_IREF            = 51,
    SET_IRESET_BIT         = 52,
    SWCNTL_DACMONV         = 53,
    SWCNTL_DACMONI         = 54,
    SET_IBUFP_MON_0        = 55,
    SET_IBUFN_MON_0        = 56,
    SET_IBUFP_MON_1        = 57,
    SET_IBUFN_MON_1        = 58,
    PULSE_MON_L            = 59,
    PULSE_MON_R            = 60,
    LVDS_VBCMFB            = 61,
    MASK_PIXEL             = 62,
    MASK_PIXELS_FROM_TXT   = 63,  
    ROI                    = 64, 
    MASK_DOUBLE_COLUMNS    = 65,
    TAP_FROM_TXT           = 66 

  };
  

  
  /**
   * @brief Create an empty Malta2 class. Initialize internal memories.
   */
  Malta2();

  /**
   * @brief Clear the internal memories.
   **/
  ~Malta2();
  
  /**
   * @brief Enable the verbose mode
   * @param enable enable verbose if true
   **/
  void SetVerbose(bool enable);

  /**
   * @brief Set the internal ipbus pointer
   * @param ipb pointer to ipbus::Uhal object
   **/

  void SetIPbus(ipbus::Uhal * ipb);

  /**
   * @brief Get the internal ipbus pointer
   * @return ipbus::Uhal pointer
   **/
  ipbus::Uhal * GetIPbus();
  
  /**
   * @brief Connect to the FPGA through ipbus
   * @param connstr Ipbus connection string: "tcp://hostname:port?target=device:port"
   * @return True if communication was established
   **/
  bool Connect(std::string connstr);
  
  /**
   * @brief Check the connection to the FPGA
   * @return true if connection seems right
   **/
  bool CheckConnection();

  /**
   * @brief force the defaults into the internal memory
   **/
  void Reset();
  
  /**
   * @brief set the default values for the slow control
   * @param force true to write values to chip
   **/
  void SetDefaults(bool force=true);
  
  /**
   * @brief Get the default value of a register
   * @param pos Register to get
   **/
  uint32_t GetDefault(int pos);

  /**
   * @brief Write a register
   * @param pos register to write
   * @param value value to write
   * @param force write immediately to the chip
   **/
  void Write(uint32_t pos, uint32_t value, bool force=true);
  
  /**
   * @brief Write all the registers to the chip from memory
   **/
  void WriteAll();
    
  /**
   * @brief read
   * @param pos register to read
   * @param force true to read from the chip
   * @return uint32_t word
   **/
  uint32_t Read(int pos, bool force=true);
  
  /**
   * @brief Read all the registers from the chip into memory
   **/
  void ReadAll();

  /**
   * @brief Print the value of the registers to screen
   * @param update force to read from the chip
   **/
  int DumpByWord(bool update=false);
  /**
   * @brief Compare data sent and data receive, count errors
   * @param Force update
   * @return number of errors
   **/
  int CheckSlowControl(bool update=false);

  void PrintFullSCWord();
  int DumpByIPbus(bool update=false);
  
  /**
   * @brief Set the state of the slow control: 0=IDLE, 1=Send to chip
   * @param val New state for the slow cotnrol controller (0=IDLE, 1=Send to chip)
   **/
  void  SetState(int val);

  /**
   * @brief Return the address of the slow control register 
   * @param the name of the slow control register
   * @return the address of the slow control register
   **/
  uint32_t Word(std::string word);

  /**
   * @brief read a given number of malta words. Please use and even number.
   * @param values the pointer where to store the values
   * @param numwords the number of words to read
   **/
  void ReadMaltaWord(uint32_t * values, uint32_t numwords);

  /**
   * @bried Read the monitoring word used for tap calibration
   * @param values the pointer where to store the word
   **/
  void ReadMonitorWord(uint32_t * values);

  /**
   * Reset only the FIFO.
   **/
  void ResetFifo();

  /**
   * @brief Send the available slow control commands to the FPGA
   **/
  void Send();

  /**
   * @brief Send and Load
   **/
  void Configure();

  /**
   * @brief Set parasitic capacitance value
   * @param capacitance in ato-Farads
   **/
  void SetCapacitance(float capacitance);

  /**
   * @brief Set parasitic capacitance value
   * @return capacitance in ato-Farads
   **/
  float GetCapacitance();
  
  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   **/
  void EnableMerger(bool enable);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   **/
  void EnableMonDacCurrent(bool enable);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   **/
  void EnableMonDacVoltage(bool enable);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   **/
  void EnableIDB(bool enable);

  /**
   * @brief Enable/Disable ITHR monitoring
   * @param enable Enable if true
   **/
  void EnableITHR(bool enable);

  /**
   * @brief Enable/Disable IBIAS monitoring
   * @param enable Enable if true
   **/
  void EnableIBIAS(bool enable);

  /**
   * @brief Enable/Disable IRESET monitoring
   * @param enable Enable if true
   **/
  void EnableIRESET(bool enable);

  /**
   * @brief Enable/Disable ICASN monitoring
   * @param enable Enable if true
   **/
  void EnableICASN(bool enable);

  /**
   * @brief Enable/Disable VCASN monitoring
   * @param enable Enable if true
   **/
  void EnableVCASN(bool enable);

  /**
   * @brief Enable/Disable VCLIP monitoring
   * @param enable Enable if true
   **/
  void EnableVCLIP(bool enable);

  /**
   * @brief Enable/Disable VPULSE_HIGH monitoring
   * @param enable Enable if true
   **/
  void EnableVPULSE_HIGH(bool enable);

  /**
   * @brief Enable/Disable VPULSE_LOW monitoring
   * @param enable Enable if true
   **/
  void EnableVPULSE_LOW(bool enable);

  /**
   * @brief Enable/Disable VRESET_D monitoring
   * @param enable Enable if true
   **/
  void EnableVRESET_D(bool enable);

  /**
   * @brief Enable/Disable VRESET_P monitoring
   * @param enable Enable if true
   **/
  void EnableVRESET_P(bool enable);

  /**
   * @brief Set DAC value for IDB
   * @param value DAC value from 0 to 255
   **/
  void SetIDB(uint32_t value);

  /**
   * @brief Set DAC value for ITHR
   * @param value DAC value from 0 to 255
   **/
  void SetITHR(uint32_t value);

  /**
   * @brief Set DAC value for IBIAS
   * @param value DAC value from 0 to 255
   **/
  void SetIBIAS(uint32_t value);

  /**
   * @brief Set DAC value for IRESET
   * @param value DAC value from 0 to 255
   **/
  void SetIRESET(uint32_t value);

  /**
   * @brief Set DAC value for ICASN
   * @param value DAC value from 0 to 255
   **/
  void SetICASN(uint32_t value);

  /**
   * @brief Set DAC value for VCASN
   * @param value DAC value from 0 to 255
   **/
  void SetVCASN(uint32_t value);

  /**
   * @brief Set DAC value for VCLIP
   * @param value DAC value from 0 to 255
   **/
  void SetVCLIP(uint32_t value);

  /**
   * @brief Set DAC value for VPULSE_HIGH
   * @param value DAC value from 0 to 255
   **/
  void SetVPULSE_HIGH(uint32_t value);

  /**
   * @brief Set DAC value for VPULSE_LOW
   * @param value DAC value from 0 to 255
   **/
  void SetVPULSE_LOW(uint32_t value);

  /**
   * @brief Set DAC value for VRESET_D
   * @param value DAC value from 0 to 255
   **/
  void SetVRESET_D(uint32_t value);

  /**
   * @brief Set DAC value for VRESET_P
   * @param value DAC value from 0 to 255
   **/
  void SetVRESET_P(uint32_t value);

  /**
   * @brief Set the value for VBCMFB used the length of the pulse
   * @param value DAC value from 0 to 16
   **/
  void SetLVDS_VBCMFB(uint32_t value);

  /**
   * @brief Set the pulse width
   * @param value pulse width in ps (500, 750, 1000, 2000)
   **/
  void SetPulseWidth(uint32_t value);

  /**
   * @brief Force the IPBUS synchronization
   **/
  void Sync();

  /**
   * @brief Read the status of the FIFOs
   */
  uint32_t ReadFifoStatus();
  
  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is full
   **/
  bool IsFifo1Full();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is empty
   **/
  bool IsFifo1Empty();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is half full
   **/
  bool IsFifo1Half();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is full
   **/
  bool IsFifo2Full();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is empty
   **/
  bool IsFifo2Empty();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is half full
   * @return true if is more than half full
   **/
  bool IsFifo2Half();

  /**
   * @brief Check if the monitoring fifo is empty
   * @return true if empty
   **/
  bool IsFifoMonEmpty();

  /**
   * @brief Check if pixel is masked
   * @return true if the pixel has been actively masked
   **/
  bool IsPixelMasked(uint32_t col, uint32_t row);

  /**
   * @brief Change a setting from a string
   * @param str string to parse. Can contain multiple lines
   * @return true if string could be parsed
   **/
  bool SetConfigFromString(const std::string& str);

  /**
   * @brief Change a setting from a file
   * @param file path to parse
   * @return true if file could be parsed
   **/
  bool SetConfigFromFile (const std::string& fileName);
  
  /**
   * Write the same tap delays for all bits
   * @param tap1 Tap value for 0 degree data copy
   * @param tap2 Tap value for 90 degree data copy
   **/
  void WriteConstDelays(uint32_t tap1, uint32_t tap2);

  /**
   * Write the tap delays for a given bit
   * @param bit Integer value of the bit
   * @param tap1 Tap value for 0 degree data copy
   * @param tap2 Tap value for 90 degree data copy
   **/
  void WriteTap(uint32_t bit, uint32_t tap1, uint32_t tap2);
  
  /**
   * Read the tap delays for a given bit
   * @param bit Integer value of the bit
   * @return vector with tap1 followed by tap2
   **/
  std::vector<uint32_t> ReadTap(uint32_t bit);
  
  /**
   * Read the taps for all bits from a file
   * @param fileName path to the file
   **/
  void ReadTapsFromFile(std::string fileName);

  /**
   * Write the taps for all bits to a file
   * @param fileName path to the file
   **/
  void WriteTapsToFile(std::string fileName);

  /**
   * @brief Generate a number of L1A internally
   * @param ntimes Number of triggers to generate
   * @param withPulse Actually trigger the pulsing
   **/
  void Trigger(uint32_t ntimes=1, bool withPulse=true);

  /**
   * @brief set readout delay, values larger than 255 are set to 255
   * @param delay the size of the pipeline
   **/
  void SetReadoutDelay(uint32_t delay);

  /**
   * @brief Get readout delay currently set on the FPGA
   * @return The size of the pipeline 
   **/
  uint32_t GetReadoutDelay();

  /**
   * @brief Enable/Disable pulsing on a given pixel
   * @param row Pixel row
   * @param column Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulse(uint32_t col, uint32_t row, bool enable);

  /**
   * @brief Enable/Disable pulsing on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulseRow(uint32_t row, bool enable);

  /**
   * @brief Enable/Disable pulsing on a given pixel column
   * @param col Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulseColumn(uint32_t col, bool enable);

  /**
   * @brief Enable/Disable mask on a given pixel
   * @param column Pixel column
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMask(uint32_t col, uint32_t row, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskRow(uint32_t row, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel column
   * @param column Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskColumn(uint32_t col, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel diagonal
   * @param diag Pixel diagonal
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskDiag(uint32_t diag, bool enable);

  /**
   * @brief Enable/Disable mask on double column
   * @param dc Pixel double column
   * @param enable Enable if true, disable if false
   **/
  void SetDoubleColumnMask(uint32_t dc, bool enable);

  /**
   * @brief Enable/Disable mask on a range of double columms
   * @param dc1 First double column to mask
   * @param dc2 Last double column to mask
   * @param mask Mask the double column in range if true, otherwise un-mask
   **/
  void SetDoubleColumnMaskRange(uint32_t dc1, uint32_t dc2, bool mask);

  /**
   * @brief Set Full mask map from the text file
   * @param fileName Absolute path for the file that contains the mask list
   **/
  void SetFullPixelMaskFromFile(std::string fileName);

  /**
   * @brief Enable/Disable the analog masking outside of this columns
   * @param col1 Mask from 0 to this column
   * @param row1 Mask from 0 to this row
   * @param col2 Mask from this column to 511
   * @param row2 Mask from this row to 511
   * @param mask Enable if true, disable if false
   **/
  void SetROI(uint32_t col1, uint32_t row1, uint32_t col2, uint32_t row2, bool mask);
  
  /**
   * @brief Enable/Disable the external L1A signal.
   * @param enable Enable the readout if true
   **/
  void SetExternalL1A(bool enable);

  /**
   * @brief Enable the small FIFO
   **/
  void ReadoutOn();

  /**
   * @brief Disable the small FIFO
   **/
  void ReadoutOff();

  /**
   * @brief Enable the output of the trigger signal
   **/
  void EnableFastSignal();

  /**
   * @brief Disable the output of the trigger signal
   **/
  void DisableFastSignal();
  
  /**
   * @brief Enable/Disable the fast signal.
   * @param enable Enable the fast signal if true
   **/
  void SetFastSignal(bool enable);

  /**
   * @brief Reset the L1ID counter
   **/
  void ResetL1Counter();
  
  /**
   * Get the current L1ID from the FIFOSTATUS register (busy register)
   * @return the current L1ID.
   **/
  uint32_t GetL1ID();

  /**
   * Get the number of words currently available to read in the IPB FIFO (FIFO2)
   * @return The number of words available in the FIFO.
   **/
  uint32_t GetFIFO2WordCount();

  /**
   * @brief returns a TH2D* histogram with all pixels masked based on configuration
   **/
  TH2I* GetPixelMask();

  /**
   * @brief returns a TH1D* histogram with all double columnds masked based on configuration
   **/
  TH1D* GetDColMask();

  /**
   * @brief returns a TH2D* histogram with all actually masked pixels including ghost masking
   **/
  TH2I* GetMaskedPixels(int roiXmin=0, int roiYmin=0, int roiXmax=0, int roiYmax=0);

  /**
   * Get the number of IPBus calls since the Malta2::Connect was called.
   * This counter cannot be reset, and it counts how many times Uhal::Send was called
   * @return the number of times an ipbus message was sent over the network
   **/
  uint32_t GetNumCalls();

  /**
   * @brief Get DAC value for IDB
   * @return DAC value from 0 to 255
   **/
  uint32_t GetIDB();

  /**
   * @brief Get DAC value for ITHR
   * @return DAC value from 0 to 255
   **/
  uint32_t GetITHR();

  /**
   * @brief Get DAC value for IBIAS
   * @return DAC value from 0 to 255
   **/
  uint32_t GetIBIAS();

  /**
   * @brief Get DAC value for IRESET
   * @return DAC value from 0 to 255
   **/
  uint32_t GetIRESET();

  /**
   * @brief Get DAC value for ICASN
   * @return DAC value from 0 to 255
   **/
  uint32_t GetICASN();

  /**
   * @brief Get DAC value for VCASN
   * @return DAC value from 0 to 255
   **/
  uint32_t GetVCASN();

  /**
   * @brief Get DAC value for VCLIP
   * @return value DAC value from 0 to 255
   **/
  uint32_t GetVCLIP();

  /**
   * @brief Get DAC value for VPULSE_HIGH
   * @return value DAC value from 0 to 255
   **/
  uint32_t GetVPULSE_HIGH();

  /**
   * @brief Get DAC value for VPULSE_LOW
   * @return value DAC value from 0 to 255
   **/
  uint32_t GetVPULSE_LOW();

  /**
   * @brief Get DAC value for VRESET_D
   * @return DAC value from 0 to 255
   **/
  uint32_t GetVRESET_D();

  /**
   * @brief Get DAC value for VRESET_P
   * @return DAC value from 0 to 255
   **/
  uint32_t GetVRESET_P();

  /**
   * @brief Get DAC value for LVDS_VBCMFB
   * @return DAC value from 0 to 255
   **/
  uint32_t GetLVDS_VBCMFB();

  /**
   * @brief Get the pulse width
   * @param pulse width in ps (500, 750, 1000, 2000)
   **/
  uint32_t GetPulseWidth();

 private:

  //! The IPBUS pointer
  ipbus::Uhal * m_ipb;

  //! Defaults for normal registers
  std::map<uint32_t, uint32_t> defs;
  
  uint32_t fifos[18]={92,220,348,476,604,732,860,988,1116,1244,1372,1500,2012,2524,3036,3292,3804,4316};  
  uint32_t m_fifoSizes[17];

  //! Non-memory efficient definition of the fifo defaults
  uint32_t defsFIFO[17][512];
 
  std::map<uint32_t, uint32_t> m_values;// to be written
  int m_fifos[17][512];//to be written
  int m_fifos_words[136];
  int m_values_words[4];

  std::map<uint32_t, uint32_t> m_values_r;// read
  int  m_fifos_r[17][512];//read
  int m_values_words_r[4];
  int m_fifos_words_r[136];
  std::map<uint32_t, std::string> names;
  std::map<std::string, uint32_t> addresses;
  std::string values_s[81];
  bool verbose;

  float m_capacitance;
  
  uint32_t m_busy;
  
  void thermomiter_encoder(int pos, uint32_t value);
  void hot_encoder(int pos, uint32_t value);
  uint32_t get_diagonal(int col, int row);

  void initialize_array();

  std::vector<uint32_t> m_mask_row;
  std::vector<uint32_t> m_mask_col;
  std::vector<uint32_t> m_mask_diag;
  std::vector<uint32_t> m_mask_dc;

  std::vector<std::pair<uint32_t,uint32_t>> m_maskedPixelList; //!< Vector of masked pixels

};
#endif

