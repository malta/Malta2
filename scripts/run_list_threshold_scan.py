#!/usr/local/bin/python

import os
from time import sleep

chip="W8R20"
address="udp://ep-ade-gw-01.cern.ch:50003"

###CONFIG
SC_VCASN= 100
SC_VCLIP= 125
SC_VRESET_P= 40
SC_VRESET_D= 56
SC_ICASN= 2
SC_ITHR= 9
SC_IBIAS= 43
SC_IDB= 125

ITHR_min = 7
ITHR_max = 20

for it in range(ITHR_min,ITHR_max+1):

    f = open("./scripts/MALTA_conf.txt", "w")
    f.write("SC_VCASN: "+str(SC_VCASN)+"\n")
    f.write("SC_VCLIP: "+str(SC_VCLIP)+"\n")
    f.write("SC_VRESET_P: "+str(SC_VRESET_P)+"\n")
    f.write("SC_VRESET_D: "+str(SC_VRESET_D)+"\n")
    f.write("SC_ICASN: "+str(SC_ICASN)+"\n")
    f.write("SC_ITHR: "+str(it)+"\n")
    f.write("SC_IBIAS: "+str(SC_IBIAS)+"\n")
    f.write("SC_IDB: "+str(SC_IDB)+"\n")
    f.close()

    paramMin = 76 ##was 70 ##VD: pls go back to 20 or so if you don't know what you are supposed to do
    paramMax = 86
    paramStep = 1

    tag = chip+"_VCASN_"+str(SC_VCASN)+"_VCLIP_"+str(SC_VCLIP)+"_VRESET_P_"+str(SC_VRESET_P)+"_VRESET_D_"+str(it)+"_ICASN_"+str(SC_ICASN)+"_ITHR_"+str(SC_ITHR)+"_IBIAS_"+str(SC_IBIAS)+"_VCLIP_"+str(SC_IDB)

    cmd = "perf record Malta2ThresholdScan " 
    cmd += " -a "+address # address
    cmd += " -o "+tag #output
    cmd += " -f "+chip # basefolder
    cmd += " -l "+str(paramMin)  # paramMin
    cmd += " -h "+str(paramMax)  # paramMax
    cmd += " -s "+str(paramStep) # paramStep
    cmd += " -r 0 512 288 224 " # region [xmin(0-511) No.x ymin(>287) No.y (0-223)] ### a b c d : starting pixel (a c), pulsing b cols and d rows
    cmd += " -t 120" # trigger
    cmd += " -e" # even
    cmd += " -q" # quiet 
    cmd += " -c ./scripts/MALTA_conf.txt" # config file

    print(cmd)
    os.system(cmd)
    sleep(1)

