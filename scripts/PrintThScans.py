#!/usr/bin/env python

import ROOT
import os
#import mysql.connector
import socket
import time
import glob
#import subprocess
#from collections import namedtuple
#from re import match
#import argparse
import os.path



baseFolder="/home/sbmuser/MaltaSW/Malta2/Results_ThScan/"

tmpList=glob.glob(baseFolder+"*")
#print (tmpList)

tmpList.sort()

print (tmpList)

for c in tmpList:
  if "W5R10" not in c: continue
  chipName=c.split("/")[-1]

  if "W5R23" not in chipName: continue

  print (" ")
  print ("===============================================================================================================================================================================================")
  print ("Analysing chip: "+chipName)
  folder=glob.glob(c+"/*")
  folder.sort()
  for r in folder:
    run=r.split("/")[-1].replace(chipName+"_","")
    #os.system("ls -la "+r)
    defFile=r+"/th_sum_IDB_.root"
    if not os.path.exists(defFile):
      print (defFile)
      print ("<!!!!! RESULT FILE NOT EXISTING FOR THIS CONFIGURATION: consider aborting !!!!!>")
      continue
    myF=ROOT.TFile(defFile)
    #myF.ls()
    cT=myF.Get("config")

    #cT.Print()
    if cT==None:
      print (defFile+" ... is correupted .... consider deleting ")
      continue
    cT.GetEntry(0)
    #ITH=0
    #try: ITH=cT.SC_ITHR
    #except:
    #  try: ITH=cT.SC_ITHR10
    #  except:
    #    try: ITH=cT.SC_ITHR15
    #    except:
    #      try: ITH=cT.SC_ITHR7
    #      except:
    #        try: ITH=cT.SC_ITHR30
    #        except:
    #          try: ITH=cT.SC_ITHR9
    #          except:
    #            try: ITH=cT.SC_ITHR12
    #            except:
    #              try: ITH=cT.SC_ITHR20
    #              except:
    #                try: ITH=cT.SC_ITHR25
    #                except:
    #                  try: ITH=cT.SC_ITHR8
    #                  except:
    #                    try: ITH=cT.SC_ITHR11
    #                    except:
    #                     cT.Print()
    #                     #sys.exit()
    ITH=cT.SC_ITHR
    IDB=cT.SC_IDB
    IBI=cT.SC_IBIAS
    ICN=cT.SC_ICASN
    VCN=cT.SC_VCASN
    VRP=cT.SC_VRESET_P
    VRD=cT.SC_VRESET_D
    VCL=0
    try: VCL=cT.SC_VCLIP
    except:
      try: VCL=cT.SC_VCLIP40
      except:
        cT.Print()
    config="ITHR: %3s, IDB: %3s, IBIAS: %3s, ICASN: %3s, VCASN: %3s, VRES_P: %3s, VRES_D: %3s, VCLIP: %3s " % (ITH,IDB,IBI,ICN,VCN,VRP,VRD,VCL)
    
    #cT.Print()
    #myF.ls()
    tT=myF.Get("Thres")
    totPix=tT.GetEntries()
    config=(" * %110s " % run.replace("_ThresholdScan","").replace("VRESET_","V").replace("2022",""))+" "+config+(", Scanned: %6s" % totPix )

    myF2=ROOT.TFile(r+"/plots.root")
    #myF2.ls()
    tH=myF2.Get("thres_1D")
    #print (tH)
    if tH==None:
      print (defFile+" ... is corrupted .... consider deleting ")
      continue
    nH=myF2.Get("noise_1D")
    good=int(tH.GetEntries())
    config+=(", good: %6s"%good)
    config+=(", Th: %3.0f +/- %2.0f" % (tH.GetMean(), tH.GetRMS()) )
    config+=(", No: %02.1f +/- %2.1f" % (nH.GetMean(), nH.GetRMS()) )
    print (config)

print (" ")
print ("DONE")
