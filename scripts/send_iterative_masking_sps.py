#!/usr/local/bin/python

# the script runs a set of pre-configured runs for different VSUB and ITHR values

import os, glob
import datetime

##################
# CHIP parameters 
##################

# chip name
chip="W12R16"
# substrate voltage (saved for booking in the config file)
vsub = 55.0 
# ITHR we want to scan
ITHRs = [20]
#ITHRs = [20, 25, 30, 40, 60, 80, 100]
# chip parameters that sometime are set differently
ICASN = 2

#any additional tag you want to add
tag = ""

# path to a config files used as a starting point
template = "configs/sps_configs/2023/W12R16/template.txt"

# sorting in reverse order
ITHRs = sorted(ITHRs, reverse=True)


###############################
# parameters of iterative scan
###############################

# the resulting config of a scan for the higher ITHR
# becomes the input (starting point) of the scan for the lower ITHR 
doAppend = False
if template: doAppend = True

# set to -1 if you just want to target a limit of the noise per pixel
chiprate_limit = -1 # in Hz 

n_maskpixel_max = 100
pixelrate_limit = 40 # in Hz

max_rate_accuracy = 2 # do not touch if not strictly needed

# suffix to be added to iterative scan directory and final config file
suffix = ""

# add suffix if parameters different from default
if chiprate_limit > 0:
    suffix += "_chipN_%.0fkHz" % (chiprate_limit / 1000)
if pixelrate_limit != 40:
    suffix += "_pixelN_%dHz" % pixelrate_limit
if n_maskpixel_max != 100:
    suffix += "_npixmax_%d" % n_maskpixel_max
if max_rate_accuracy != 2:
    suffix += "_nacc_%dHz" % max_rate_accuracy

if chiprate_limit < 0:
    print('\033[1m\033[92mIterative will just care of the noise limit of %d Hz per single pixel.\033[0m' % pixelrate_limit)
    print('\033[1m\033[92mThe full chip noise limit is ignored.\033[0m')

if chiprate_limit > 0:

    if chiprate_limit < pixelrate_limit:
        raise RuntimeError('\033[1m\033[95mYou chip rate limit is less than the single pixel rate limit!\033[0m')
    
    if chiprate_limit * 1e3 / max_rate_accuracy < 10:
        raise RuntimeError('\033[1m\033[95mYou want to mask pixels with a total chip rate limit of %d Hz and an accuracy of only %d Hz...\nPlease provide a better accuracy\033[0m' % (pixelrate_limit, max_rate_accuracy))

if pixelrate_limit / max_rate_accuracy < 10:
    raise RuntimeError('\033[1m\033[95mYou want to mask pixels with rate %d Hz with an accuracy of only %d Hz...\nPlease provide a better accuracy\033[0m' % (pixelrate_limit, max_rate_accuracy))



print ("\n%---------------------------------------%")
print (" chip: %s" % chip)
if chiprate_limit > 0:
    print (" chip noise limit: %d kHz" % (chiprate_limit / 1000))
print (" pixel noise limit: %d Hz" % pixelrate_limit)
print (" max number of maskable pixels: %d" % n_maskpixel_max)
print (" max rate accuracy: %d Hz" % max_rate_accuracy)
print ("%---------------------------------------%\n")



address="udp://ep-ade-gw-02:50008"

# date
now = datetime.datetime.now()
date_str = now.strftime('%Y%m%d')

tmp_nlines = 0
for i, ITHR in enumerate(sorted(ITHRs, reverse=True)):

    print('\n\033[1m\033[92mStarting with ITHR %d.\033[0m\n' % ITHR)

    config_dir = "configs/sps_configs/2023/" + chip
    #if tag:
    #    config_dir += "_" + tag
    os.makedirs(config_dir, exist_ok=True)

    filename = "config_" + chip + "_ITHR_" + str(ITHR) + suffix
    if tag:
        filename += "_" + tag
    filename += ".txt"

    # generate the first config file
    if i == 0 and not doAppend:
        print ("GIULIANO")
        #continue
        with open(config_dir + "/" + filename, "w") as f:

            f.write("#Info type: MALTA2\n")
            f.write("#Info sample: " + chip + "\n")
            f.write("#Info VSUB: %.1f V\n" % vsub)
            if chiprate_limit > 0:
                f.write("#Info chip noise limit: %d kHz\n" % (chiprate_limit / 1000))
            f.write("#Info pixel noise limit: %d Hz\n" % pixelrate_limit)
            f.write("#Info max number of maskable pixels: %d\n" % n_maskpixel_max)
            f.write("#Info max rate accuracy: %d Hz\n" % max_rate_accuracy)
            f.write("SC_ICASN: " + str(ICASN) + "\n")
            f.write("SC_IBIAS: 43\n")
            f.write("SC_ITHR: " + str(ITHR) + "\n")
            f.write("SC_IDB: 120 \n")
            f.write("SC_VCASN: 110\n")
            f.write("SC_VRESET_P: 29\n")
            f.write("SC_VRESET_D: 65\n")
            f.write("SC_VCLIP: 125\n")
            f.write("#Iterative masking from ITHR: %d\n" % ITHR)


    # the resulting config of a scan for the higher ITHR
    # becomes the input (starting point) of the scan for the lower ITHR
    elif (i > 0 and not template) or template: 

        # the template config is used just for the first entry in the loop
        if template and i == 0:
            old_config = template 
        else:
            old_config = config_dir + "/" + "config_" + chip + "_ITHR_" + str(ITHRs[i-1]) + ".txt"

        with open(old_config, 'r') as f:
            file_contents = f.read()

        # Split the file contents into lines
        lines = file_contents.split('\n')

        # Loop through the lines and find the line with SC_ITHR
        for l, line in enumerate(lines):

            if line.startswith('SC_ITHR:'):
                # Get the current value of SC_ITHR
                current_value = int(line.split(':')[1].strip())
                
                # Replace the line with the new value
                lines[l] = 'SC_ITHR: ' + str(ITHR)

        # Join the lines and write the new config
        new_file_contents = '\n'.join(lines)
        new_file_contents += "#Iterative masking from ITHR: %d\n" % ITHR 

        with open(config_dir + "/" + filename, 'w') as f:
            f.write(new_file_contents)
            print(new_file_contents)

    # launching the iterative scan       
    outdir = date_str + "_" + chip + "_IBIAS_43_ITHR_" + str(ITHR) + "_IDB_120_ICASN_" + str(ICASN) + "_VCASN_110_VRESET_P_29_VRESET_D_65_IterativeScanSPS" + ("_VSUB_%.1f" % vsub).replace('.','p')
    outdir += suffix
    if tag:
        outdir += "_" + tag

    cmd_scan = "python scripts/run_iterative_masking_sps.py " 
    cmd_scan += " -a " + address # address
    cmd_scan += " -o " + outdir #output
    cmd_scan += " -f " + chip # basefolder
    cmd_scan += " -l %d" % chiprate_limit
    cmd_scan += " -p %d" % pixelrate_limit
    cmd_scan += " -r %d " % max_rate_accuracy
    cmd_scan += " -m %d" % n_maskpixel_max
    cmd_scan += " -c " + config_dir + "/" + filename

    print(cmd_scan)
    os.system(cmd_scan)

    ###########################################################
    # copy and substitute the output of the
    # iterative scan as a new config for the SPS test campaign
    ###########################################################

    with open("Results_NoiseScan/" + chip + "/" + outdir + "/Mask_IDB120_new.txt", "r") as f:
        lines = f.readlines()
    
    # Extract eventual errors
    error_lines = [line for line in lines if 'ERROR' in line]
    if len(error_lines):
        raise RuntimeError('Iterative scan with ITHR = %d exited with an ERROR' % ITHR)

    # Line from which we want to edit the file
    idx = next((i for i, line in enumerate(lines) if ("#Iterative masking from ITHR: %d" % ITHR) in line), None)

    if idx == None: 
        raise RuntimeError("Error in parsing the output file " +  "Results_NoiseScan/" + chip + "/" + outdir + "/Mask_IDB120_new.txt")

    old_lines = lines[:idx+1]
    old_lines = [line.strip() for line in old_lines]
    old_lines.insert(-1, "#")

    new_lines = lines[idx+1:]

    # Remove empty lines and lines starting with "#The noise rate"
    new_lines = [line.strip() for line in new_lines if line.strip() != "" and not line.startswith("#The noise rate")]
    
    # Extract the setup lines (this should work only for i == 0)
    setup_lines = [line for line in new_lines if line.startswith("SC_") or line.startswith("#Info")]
    
    # Extract the lines with MASK_DOUBLE_COLUMN and MASK_PIXEL
    mask_double_column_lines = [line for line in new_lines if line.startswith("MASK_DOUBLE_COLUMN")]

    mask_pixel_lines = [line for line in new_lines if line.startswith("MASK_PIXEL")]
    mask_pixel_lines = [line.split("|", 1)[0] for line in mask_pixel_lines]

    #Extract summary lines
    summary_lines = [line for line in new_lines if line.startswith("SC_") or line.startswith("#Summary")]

    # Sort the lines by type (setup lines first, then MASK_DOUBLE_COLUMN, then MASK_PIXEL)
    lines_sorted = old_lines + setup_lines + mask_double_column_lines + mask_pixel_lines + summary_lines
    
    # Write the output to a new file
    with open(config_dir + "/" + filename, "w") as out_f:
        for line in lines_sorted:
            out_f.write(line + "\n")

        
        
        
