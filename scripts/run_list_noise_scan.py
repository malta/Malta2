#!/usr/local/bin/python

import os
from time import sleep

irr_point = "ITHR6_2"
chip="W8R20"
address="udp://ep-ade-gw-01.cern.ch:50003"

###CONFIG
SC_VCASN= 100
SC_VCLIP= 125
SC_VRESET_P= 40
SC_VRESET_D= 56
SC_ICASN= 2
SC_ITHR= 9
SC_IBIAS= 43
SC_IDB= 125

ITHR_min = 6
ITHR_max = 6

paramMin = 125
paramMax = 126
paramStep = 20
acq_time = 100
reps = 10

for it in range(ITHR_min,ITHR_max+1):
    for i in range(0,20):

        f = open("./scripts/MALTA_conf.txt", "w")
        f.write("SC_VCASN: "+str(SC_VCASN)+"\n")
        f.write("SC_VCLIP: "+str(SC_VCLIP)+"\n")
        f.write("SC_VRESET_P: "+str(SC_VRESET_P)+"\n")
        f.write("SC_VRESET_D: "+str(SC_VRESET_D)+"\n")
        f.write("SC_ICASN: "+str(SC_ICASN)+"\n")
        f.write("SC_ITHR: "+str(it)+"\n")
        f.write("SC_IBIAS: "+str(SC_IBIAS)+"\n")
        f.write("SC_IDB: "+str(SC_IDB)+"\n")
        f.close()

        tag = chip+"_iter_"+str(i)+"_VCASN_"+str(SC_VCASN)+"_VCLIP_"+str(SC_VCLIP)+"_VRESET_P_"+str(SC_VRESET_P)+"_VRESET_D_"+str(SC_VRESET_D)+"_ICASN_"+str(SC_ICASN)+"_ITHR_"+str(it)+"_IBIAS_"+str(SC_IBIAS)+"_VCLIP_"+str(SC_IDB)

        cmd = "Malta2NoiseScan "
        cmd += " -o "+tag #output
        cmd += " -f "+chip+"/"+irr_point+"/" # basefolder
        cmd += " -a "+address # address
        cmd += " -l "+str(paramMin)  # paramMin
        cmd += " -h "+str(paramMax)  # paramMax
        cmd += " -t "+str(acq_time) # acquisition time
        cmd += " -r "+str(reps) # repetitions
        cmd += " -s "+str(paramStep) # paramStep
        cmd += " -c ./scripts/MALTA_conf.txt" # config file

        #cmd += " | tee ./home/sbmuser/MaltaSW/Malta2/Results_NoiseScan/"+chip+"/"+tag+"/log.txt"
        cmd += " | tee ./"+tag+"_"+irr_point+"_log.txt"

        print(cmd)
        os.system(cmd)
        sleep(5)

