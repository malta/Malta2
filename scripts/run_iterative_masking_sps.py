# The script aims to mask the noisy pixels until
# - the noise rate is below a defined threshold [limit]
# - the chip has no pixels with a noise above a defined  threshold [limit_pp]
# - the number of masked pixels is below defined numbers [maxpixels]
#   - NB it includes double columns but not ghosts
# All this is done at a defined rate accuracy [rate_accuracy].
#
# The logic of the script is the following.
# 1) at first a set of noise scan are run in order to define an 'optimal' time window
#    - to avoid to full the FIFO
#    - to speed up the process but maintaing at the same time a good counting statistics (i.e. good rate accuracy)
# 2) a set of noise scans are run iteratively masking 1 pixel per cycle maintaining a sufficiently good rate accuracy ([counts_min] counts for the noisiest pixel) for the level of the chip noise at each iteration.
#   2a) if the rate accuracy is not sufficient because the chip has become more quiet but still does not fullfill the aforementioned criterion, the time window is widened, hence increasing the rate accuracy.
#       The iteration continues until the chip noise fullfills the aforementioned criteria or if the accuracy is at its limit.
#   2b) if a pixel results non maskable after 3 iteration, if in the top X list of the noisiest pixels there is one lying in the same group, it is masked
#   2c) if a pixel results non maskable after X iteration, the double column is masked. 
#   2d) if even after masking the double column, the pixel results not masked after other [n_maskpixel_max] iterations, the code creates an exemption and crashes.
# 3) the script provides as an output a config file with the chip settings and the list of masked pixels

import os
import glob
import argparse
import time
import re
import ROOT
import ctypes

# import the copyfile function from the shutil module
from shutil import copyfile

# import the path function from the os module
from os import path

# Number of pixels in the chip
n_pix_axis = 512
n_pix_x = 512
n_pix_y = 224 # MALTA2
first_pix_x = n_pix_axis - n_pix_x
first_pix_y = n_pix_axis - n_pix_y

chilo = 1e3
mega = chilo * chilo

# Number of attempts to mask a pixel
# prevents code from being stuck on same "unmaskable" pixel and exits before nmax is reached
mask_attempts = {}
mask_attempts_max = 3 + 15 # 3 attempts to try to mask the pixel, other 15 to mask the neighbour lying on the same group, otherwise mask the double column
mask_neighbours = {}

# minimum number of hits to mask pixel
# (it is an upper bound on the stat error)
counts_min = 10

# create an argument parser object
parser = argparse.ArgumentParser()

# add arguments to the parser object
parser.add_argument("-f", "--folder", type=str, default='W7R12', help="specify main folder")
parser.add_argument("-o", "--output", type=str, default='testanalysis', help="specify sub-folder")
parser.add_argument("-a", "--address", type=str, default='udp://ep-ade-gw-04.cern.ch:50002', help="specify chip address")
parser.add_argument("-c", "--configFile", type=str, default='configs/config_W7R12_IDB127_ITHR10.txt', help="specify config file")
parser.add_argument("-m", "--maxpixels", type=int, default=100, help="specify max number of pixels to mask, default = 500")
parser.add_argument("-l", "--limit", type=int, default=1000, help="max rate (in Hz) of the entire chip allowed")
parser.add_argument("-p", "--limit_pp", type=int, default=1, help="max rate (in Hz) of a single pixel")
parser.add_argument("-r", "--rate_accuracy", type=int, default=10, help="rate accuracy in Hz, default = 10")
#parser.add_argument("-r", "--repetitions", type=int, default=50, help="number of repetitions, default = 50")
#parser.add_argument("-t", "--triggers", type=int, default=10000, help="number of triggers, default = 10000")
#parser.add_argument("-n","--npixels", type=int, default=1, help="specify number of pixels that you want mask per iteration")

# parse the arguments and store them in the args object
args = parser.parse_args()

# Assigning arguments to variables for better readability
target_rate_accuracy = args.rate_accuracy # in Hz. Each count will correspond to X Hz
refR = 20 # repetitions
refT = 1. / (target_rate_accuracy * refR / mega) # time window
#refR=args.repetitions
#refT=args.triggers
#target_rate_accuracy = 1. / (refT * refR / mega) # in Hz. Each count will correspond to X Hz
path_output = './Results_NoiseScan/' + args.folder + '/' + args.output + '/'

######################
# Initialise values
######################
noise_rate = 99999.
n_pix_masked = 0
hasVeryNoisyPixel =  False
noisiest_rate = 0

# Set the maximum noise rate to the value specified by the user
limit = args.limit / chilo # expressed in kHz

# Set the maximum noise per pixel
limit_pp = args.limit_pp # expressed in Hz 

# remove all the .txt files from the output directory
if len(glob.glob(path_output + "*.txt")) > 0:
    os.system("rm " + path_output + "*.txt")

# create a TGraph object to store the pixel noise rate data
gr_pixel = ROOT.TGraph()
gr_pixel.SetNameTitle("noiseistpix_vs_masked", "noisesetipix_vs_masked;Pixels masked;Pixel Noise Rate [kHz]")

# create a TGraphErrors object to store the chip noise rate data
gr_module = ROOT.TGraphErrors()
gr_module.SetNameTitle("noise_vs_masked", "noise_vs_masked;Pixels masked;Chip Noise Rate [kHz]")

# Extracting the value of IDB from the configuration file
IDB_val = 0
config = open(args.configFile, 'r')

for x in config.readlines():
    if 'IDB' in x:
        x = re.findall(r'\b\d+\b', x)
        print("IDB value from config file: " + x[0])
        IDB_val = x[0]

config.seek(0)

# set a temporary variable to the config file path
# NB: It is an important variable that is updated at each cycle in the loop
tmp_config = args.configFile

# function to read noise file
def read_noise_file (noise_file_name):
    if not os.path.isfile(noise_file_name):
        raise RuntimeError('No file %s exists!' % (noise_file_name))
    f = ROOT.TFile(noise_file_name)
    return f

# check if the FIFO was not full
def is_fifo_full (n_file):
    n_tree = n_file.Get('noise_tree')
    n_tree.GetEntry(0)
    return n_tree.isFullFIFO

# function to remove the last masked pixel from the list
def remove_pixel_from_masked_txtlist(pix_to_remove, filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    with open(filename, 'w') as f:
        f.truncate(0)
        for id_line, line in enumerate(lines):
            if 'MASK_PIXEL: %d, %d' % (pix_to_remove[0], pix_to_remove[1]) not in line:
                f.write(line)           

# function to get the N bins with higher content in the 2D map
def get_top_n(hist, n_noisiest):
    max_bins = []
    for bin in range(1, hist.GetNbinsX() * hist.GetNbinsY() + 1):
        content = hist.GetBinContent(bin)
        if content > 0:
            if len(max_bins) < n_noisiest:
                max_bins.append((content, bin))
                max_bins = sorted(max_bins, key=lambda x: x[0], reverse=True)
            elif content > max_bins[-1][0]:
                max_bins[-1] = (content, bin)
                max_bins = sorted(max_bins, key=lambda x: x[0], reverse=True)
    # print coordinates of maximum bins
    topN = []
    for i, (content, bin) in enumerate(max_bins):
        x_c = ctypes.c_int()
        y_c = ctypes.c_int()
        z_c = ctypes.c_int()
        hist.GetBinXYZ(bin, x_c, y_c, z_c)
        topN.append((int(hist.GetXaxis().GetBinCenter(x_c.value)), 
                     int(hist.GetYaxis().GetBinCenter(y_c.value))))
    return topN

####################################
# Iterate to avoid to full the FIFO
####################################

#refRate=0.

isGood=False

# Starting with a trigger choice 
# to have an estimate of the noise
tmp_t = round(1e5/refR) #GG here we set as a total time 0.1s => each count is 10 Hz accuracy

while not isGood:
    
    
    print ("RefT is: :"+str(tmp_t))
    
    # construct command to run the noise scan
    command=' Malta2NoiseScan -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(tmp_t)+' -r '+str(refR)+' -s 10 -c ' + tmp_config + ' -n 1'
    print (command)
    
    # execute noise scan command
    os.system(command)
    
    # open noise file
    noise_file = read_noise_file(path_output + 'noise_summary.root')
    
    # total chip noise in kHz
    h_occ  = noise_file.Get("Occupancy_IDB_%d" % int(IDB_val))
    tot_time = tmp_t * refR / mega # in s
    rate_accuracy = 1. / tot_time
    noise_rate = h_occ.Integral() / tot_time / chilo
    #h_pix_masked = noise_file.Get("MaskedPix")
    #n_pix_masked = h_pix_masked.Integral() #+ n_dcol_masked * 2 * n_pix_y
    
    if not is_fifo_full(noise_file):
        isGood = True

        # counts of noisiest pixel
        max_bin = h_occ.GetMaximumBin()
        max_content = h_occ.GetBinContent(max_bin)
        #if max_content > limit_pp: 
        #    hasVeryNoisyPixel = True
        # minimum counts for the noisiest pixel
        divider = max_content / (counts_min * 10) # let's be generous with minimum counts to start
        if not divider: divider = 1. # in the case when no counts are found we keep 10 Hz as a starting reference for the accuracy

        tmp_t = round(tmp_t / divider)
        if tmp_t < 5: 
            tmp_t = 5 # you don't want to go lower (Malta2NoiseScan complaints if < 3us since the DAQ window is 2.5us)
        elif tmp_t > refT:
            tmp_t = refT

    else:
        tmp_t /= 2
        if tmp_t < 1: 
            raise RuntimeError('Too small trigger time and FIFO is still full')

print ("I found my reference trigger: "+str(tmp_t) )

# Remove any existing text files in the output directory
os.system("rm " + path_output + "*.txt")


###############################################
# From here we start masking pixels iteratively
###############################################

if limit > 0:
    print("\033[1m \033[92m"+"target rate at "+str(limit)+" kHz "+" \033[0m")
else:
    print("\033[1m \033[92m"+"target rate at "+str(limit_pp)+" Hz per pixel"+" \033[0m")

# Loop until either 
# - the noise rate drops below the maximum 
# - the chip has at least a very noisy pixel
# - the maximum number of pixels is reached

needMasking = True # first try

while needMasking and n_pix_masked < args.maxpixels:
    
    # Calculate the values of t and r based on multT and refT/refR    
    #t = refT*int(multT)
    t = round(tmp_t)
    r = refR
    
    # Construct the command to execute the noise scan
    command=' Malta2NoiseScan -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(t)+' -r '+str(r)+' -s 10 -c ' + tmp_config + ' -n 1'
    
    # Print the command being executed to the console
    print (" ")
    print (command)
    
    # Execute the noise scan
    os.system( command )
    
    # read noise file and histogram to get the number of pixel masked w/ ghosts
    noise_file = read_noise_file(path_output + 'noise_summary.root')

    if is_fifo_full(noise_file):
        raise RuntimeError('the FIFO is Full during the iteration scan!?')


    h_occ  = noise_file.Get("Occupancy_IDB_%d" % int(IDB_val))
    
    # masked pixels                                                                                                  
    h_dcol_masked = noise_file.Get("dcolmask")
    h_pix_masked = noise_file.Get("MaskedPix")
    h_pix_ghosts_masked = noise_file.Get("MaskedPixGhosts")
    n_dcol_masked = h_dcol_masked.Integral()
    n_pix_masked = h_pix_masked.Integral() #+ n_dcol_masked * 2 * n_pix_y
    n_pix_ghosts_masked = h_pix_ghosts_masked.Integral()
    
    # calculating total acquisition time in seconds                                                                  
    tot_time = t * r / mega
    rate_accuracy = 1. / tot_time

    # total chip noise in kHz
    noise_rate = h_occ.Integral() / tot_time / chilo
    
    # average noise per pixel in Hz
    avg_noise_pp = noise_rate / (n_pix_x * n_pix_y - n_pix_ghosts_masked) * chilo
    
    list_pix_masked = []
    list_dc_masked = []

    ###########################
    # read the masked pixel map
    ###########################
    for x in range(first_pix_x, n_pix_axis): 
        for y in range(first_pix_y, n_pix_axis): 
    
            if h_pix_masked.GetBinContent(x+1, y+1):
                list_pix_masked.append((x, y))

    # commenting out the following check if the double column are counts in the number of pixel masked
    if len(list_pix_masked) != n_pix_masked: 
        raise RuntimeError('len(list_pix_masked) != n_pix_masked!')

    ##################
    # read the hit map
    ##################

    max_bin = h_occ.GetMaximumBin()
    max_content = h_occ.GetBinContent(max_bin)

    x_coord = ctypes.c_int()
    y_coord = ctypes.c_int()
    z_coord = ctypes.c_int()
    h_occ.GetBinXYZ(max_bin, x_coord, y_coord, z_coord)

    noisiest_rate = max_content / tot_time # in Hz
    noisiest_pix = (int(h_occ.GetXaxis().GetBinCenter(x_coord.value)), 
                   int(h_occ.GetXaxis().GetBinCenter(y_coord.value)))

    # check if there are enough counts (enough stats)
    if max_content  < counts_min: 

        remove_pixel_from_masked_txtlist(noisiest_pix, path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt')

        if rate_accuracy > target_rate_accuracy:
            hasVeryNoisyPixel = True # to continue the loop
            tmp_t = tmp_t * 10 # increase time window (NB it needs to be a multiple of 2.5 us (single DAQ time window))
            if 1. / (tmp_t * r / mega) <  target_rate_accuracy: 
                tmp_t = 1. / (target_rate_accuracy * r / mega)
            print('\033[1m\033[95mNot enough hits to mask the pixel (%d, %d) with noise %d Hz!\033[0m' % (noisiest_pix[0], noisiest_pix[1], noisiest_rate))
            print('\033[1m\033[95mI will make another iteration to have more stats\033[0m')
            #raise RuntimeError('\033[1m\033[95mNot enough hits to mask the pixel (%d, %d)!\033[0m' % (noisiest_pix[0], noisiest_pix[1]))
            continue
        else:
            # let's end the iteration 
            # we cannot mask anymore with a good enough accuracy
            break

    # if we have enough stats, we just care about the rate of the single pixels and it is below threshold
    # we remove the last masked pixel from the list and we end the scan
    if limit <= 0 and noisiest_rate < limit_pp:
        remove_pixel_from_masked_txtlist(noisiest_pix, path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt')
        needMasking = False
        print('We can stop here!')
        print('Noisiest pixel (%d, %d) has noise %.3f Hz' % (noisiest_pix[0], noisiest_pix[1], noisiest_rate))
        continue

    # check if there is a very noisy single pixel
    if noisiest_rate > limit_pp: 
        hasVeryNoisyPixel = True
    else:
        hasVeryNoisyPixel = False

    #############################
    # check for unmaskable pixels
    #############################
    if noisiest_pix in list_pix_masked:

        if noisiest_pix not in mask_attempts.keys(): 
            mask_attempts[noisiest_pix] = 1
        else:
            mask_attempts[noisiest_pix] += 1

        remove_pixel_from_masked_txtlist(noisiest_pix, path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt')

        # try with masking a pixel that xtalks with the noisiest
        # it needs to stay in the same group
        if mask_attempts[noisiest_pix] >= 3 and mask_attempts[noisiest_pix] < mask_attempts_max:
            top_noisiest = get_top_n(h_occ, 20)
            # check if it is in the same group
            group_size_x = 2
            group_size_y = 8
            in_same_group = False

            #print ('\n\n GIULIANOO list_pix_masked', noisiest_pix, list_pix_masked)
            #if noisiest_pix in mask_neighbours.keys(): print ('\n\n GIULIANOO mask_neighbours', noisiest_pix, mask_neighbours[noisiest_pix])
            #print ('\n\n GIULIANOO top_noisiest', top_noisiest)
            for p in top_noisiest: 
                
                # TODO: to be checked futher
                # check if the pixels was already tried to mask w/o solving the issue
                if p in list_pix_masked or (noisiest_pix in mask_neighbours.keys() and p in mask_neighbours[noisiest_pix]):
                    # if we already attempted to mask this pixel
                    # then we remove it from the txt list 
                    if p in list_pix_masked:
                        remove_pixel_from_masked_txtlist(p, path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt')
                        print('Removed pixel  %d, %d from the txt list of masked pixels' % (p[0], p[1])) 
                    continue

                # check if it is in the same group
                if noisiest_pix[0] // group_size_x == p[0] // group_size_x and \
                   noisiest_pix[1] // group_size_y == p[1] // group_size_y:
                    in_same_group = True
                    with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', 'a') as f:
                        f.write("MASK_PIXEL: %d, %d # neighbour of %d, %d\n" % (p[0], p[1], noisiest_pix[0], noisiest_pix[1]))
                    print ('\033[1m\033[95mThe Pixel (%d, %d) could not be masked after %d attempts.!\033[0m' % (noisiest_pix[0], noisiest_pix[1], mask_attempts[noisiest_pix]))
                    print ('\033[1m\033[95mMasking (%d, %d) which is noisy and in the same group\033[0m' % (p[0], p[1]))

                    if noisiest_pix not in mask_neighbours.keys(): 
                        mask_neighbours[noisiest_pix] = [p]
                    else: 
                        mask_neighbours[noisiest_pix].append(p)
                    break # the list is sorted so just keep the first

        # try with masking the double column
        elif mask_attempts[noisiest_pix] == round(mask_attempts_max):
            with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', 'a') as f:
                f.write("MASK_DOUBLE_COLUMN: %d\n" % (noisiest_pix[0] // 2))
            print ('\033[1m\033[95mThe Pixel (%d, %d) could not be masked after %d attempts.!\033[0m' % (noisiest_pix[0], noisiest_pix[1], mask_attempts[noisiest_pix]))
            print('\033[1m\033[95mMasking double column %d\033[0m' % (noisiest_pix[0] // 2))

            # remove the pixels masked lying on the same double column just masked!
            tmp_lines = []
            with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', "r") as f:
                tmp_lines = f.readlines()
                tmp_lines = [line for line in tmp_lines if not line.startswith("MASK_PIXEL: %d" % ((noisiest_pix[0] // 2) * 2))]
                tmp_lines = [line for line in tmp_lines if not line.startswith("MASK_PIXEL: %d" % ((noisiest_pix[0] // 2) * 2 + 1))]
                #tmp_lines = [line for line in tmp_lines if not line.startswith("#The noise rate of the entire chip is")]
            with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', "w") as f:
                f.writelines(tmp_lines)
        # if masking the dc does not work exit with an error
        elif mask_attempts[noisiest_pix] > mask_attempts_max:
            with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', 'a') as f:
                f.write("#ERROR: The iterative scan is STUCK! Pixel (%d, %d) cannot be masked!"  % (noisiest_pix[0], noisiest_pix[1]))
            raise RuntimeError('\033[1m\033[95mThe iterative scan is STUCK! Pixel (%d, %d) cannot be masked!\033[0m' % (noisiest_pix[0], noisiest_pix[1]))

    # Set the noise rate as a data point in the graph for the pixel
    gr_pixel.AddPoint(n_pix_masked, noisiest_rate)

    # Set the noise rate as a data point in the graph for the module
    gr_module.AddPoint(n_pix_masked, noise_rate)
    gr_module.SetPointError(gr_module.GetN()-1, 0, ROOT.TMath.Sqrt(noise_rate) / (t * r / chilo))
    
    
    if not noise_rate:
        print("\033[1m\033[95m" + "Chip Noise <  %.3f"  %  (1. / (t * r / chilo)) + " kHz [no hit detected]\033[0m")
        print("\033[1m\033[95m" + "Please consider to increase the acquisition time if you need a better accuracy..." + " \033[0m")
        break
    else:
        print (" ")
        print ("Conditions in the loop:: Chip Noise = " + str(noise_rate)+" kHz; N masked pixels = " + str(n_pix_masked))
        print (" ")
    
    # setting the output of noise scale as the new config file
    tmp_config =  path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt'
    
    needMasking = (noise_rate > limit and limit > 0) or hasVeryNoisyPixel


if noisiest_rate:
    with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', 'a') as f:
        f.write("#Summary info -> noise chip reached: %.3f kHz\n"  % noise_rate)
        f.write("#Summary info -> noisiest pixel (%d, %d) with rate: %d Hz\n"  % (noisiest_pix[0], noisiest_pix[1], noisiest_rate))
        f.write("#Summary info -> number of masked pixels: %d\n"  % n_pix_masked)
else:
    with open(path_output + 'Mask_IDB'+str(IDB_val)+'_new.txt', 'a') as f:
        f.write("#Summary info -> noise chip reached: %.3f kHz\n"  % noise_rate)
        f.write("#Summary info -> there is no pixel with rate above %d Hz\n"  % limit_pp)
        f.write("#Summary info -> number of masked pixels: %d\n"  % n_pix_masked)
    

gr_pixel.SetLineColor(2)
gr_module.SetLineColor(4)
gr_pixel.SetMarkerColor(2)
gr_module.SetMarkerColor(4)
gr_pixel.SetLineWidth(2)
gr_module.SetLineWidth(2)

# Create a canvas for the pixel graph and draw it
canPix = ROOT.TCanvas()
gr_pixel.Draw()
canPix.SetLogy()
canPix.Print(path_output + "noisiest_masked.pdf")

# Create a canvas for the module graph and draw it
canModule = ROOT.TCanvas()
gr_module.SetMarkerColor(4)
gr_module.SetMarkerSize(1.2)
gr_module.SetMarkerStyle(20)
gr_module.Draw("APL")
canModule.SetLogy()
canModule.Print(path_output + "noise_masked.pdf")

# Create a ROOT file to store the pixel and module graphs and associated canvases
file_output = ROOT.TFile(path_output + "noise_masked_pix.root","recreate")
file_output.cd()	
gr_pixel.Write()
gr_module.Write()
canPix.Write()
canModule.Write()
file_output.Close()
