#!/usr/bin/env python
import os
import argparse
import time
from shutil import copyfile
import re
import ROOT 
import os.path
from os import path

parser = argparse.ArgumentParser()
parser.add_argument("-f","--folder"      , type=str           , default='W7R12', help="specify main folder ")
parser.add_argument("-o","--output"      , type=str           , default='testanalysis', help="specify sub-folder")
parser.add_argument("-a","--address"     , type=str           , default='udp://ep-ade-gw-04.cern.ch:50002', help="specify chip address udp://ep-ade-gw-07.cern.ch:5000x")
parser.add_argument("-c","--configFile"  , type=str           , default='configs/Malta2_example.txt', help="specify config file")
#parser.add_argument("-n","--npixels"     , type=int           , default=1 , help="specify number of pixels that you want mask per iteration")
parser.add_argument("-m","--maxpixels"   , type=int           , default=100 , help="specify number of max number of pixels to mask, default = 100")
parser.add_argument("-M","--mask"   , type=str           , default='Mask_example.txt' , help="specify txt file with list of pixels to be masked")
args = parser.parse_args()

refR=50
refT=10000 #10

isGood=False


npixel_masked = 0


os.system("rm ../data/Malta2/Results_NoiseScan/"+args.folder+"/"+args.output+"/*.txt")

gr_pixel=ROOT.TGraph()
gr_pixel.SetNameTitle("noiseistpix_vs_masked", "noisesetipix_vs_masked;Pixels masked;Pixel Noise Rate [kHz]")

gr_module=ROOT.TGraphErrors()
gr_module.SetNameTitle("noise_vs_masked", "noise_vs_masked;Pixels masked;Chip Noise Rate [kHz]")

#extract value of IDB from config File
IDB_val = 0
config=open(args.configFile,'r')
for x in config.readlines():
  if 'IDB' in x:
    x= re.findall(r'\b\d+\b' , x)
    print ("IDB value from config file " + x[0])
    IDB_val= x[0]
config.seek(0)

###IDB_val=50

tmp_config = args.configFile

refRate=0.
while not isGood:
  refT=refT/2
  print ("RefT is: :"+str(refT))
  command=' Malta2NoiseScan -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(refT)+' -r '+str(refR)+' -s 10 -c ' + tmp_config + ' -n 1'# -M + args.mask
  #command=' MaltaNoiseScan -T Results_TapCalib/W1R1_WTN/configurationW1R1WTN_calib_run/calib_15.txt  -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(refT)+' -r '+str(refR)+' -s 10 -c ' + tmp_config + ' -n 1'
  print (command)
  os.system(command)

  noisy_pixel=open('../data/Malta2/Results_NoiseScan/'+args.folder+'/'+args.output+'/Mask_IDB'+str(IDB_val)+'_new.txt') 

  lineList = noisy_pixel.readlines()
  #  print (lineList)

  if 'chip' in lineList[-2]: 
    isGood=True
    refRate=float(lineList[-2].split("kHz")[0].split("is:")[1])#float(chip[0])
    if refRate<1: refRate=1;
  elif 'chip' in lineList[-1]:
    refRate=float(lineList[-1].split("kHz")[0].split("is:")[1])#float(chip[0])
    if refRate<1:
      refRate=1
      isGood=True
  noisy_pixel.close()

########################################################################################################
########################################################################################################
print ("I found my refT: "+str(refT) )

os.system("rm ../data/Malta2/Results_NoiseScan/"+args.folder+"/"+args.output+"/*.txt")

noise_rate=100000.
limit = 1.0 # expressed in kHz
print("\033[1m \033[92m"+"target rate at "+str(limit)+" kHz "+" \033[95m \033[0m")
while noise_rate > limit and npixel_masked < args.maxpixels:
  
  multR=1
  multT=1
  if npixel_masked!=0: multT=refRate/noise_rate
  if multT>4: multT=4.
  if multT<1: multT=1.
  if int(multT)==0: multT=1.
  t = refT*int(multT)
  r = refR

  command=' Malta2NoiseScan -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(t)+' -r '+str(r)+' -s 10 -c ' + tmp_config + ' -n 1'# -M ' + args.mask
  #command=' MaltaNoiseScan -T Results_TapCalib/W1R1_WTN/configurationW1R1WTN_calib_run/calib_15.txt -q -y -o '+args.output+' -f '+args.folder+' -a '+ args.address +' -l ' +str(int(IDB_val)-1)+'  -h ' +str(int(IDB_val))+' -t '+str(t)+' -r '+str(r)+' -s 10 -c ' + tmp_config + ' -n 1'
  print (" ")
  print(tmp_config)
  print (command)
  os.system( command )

  noisy_pixel=open('../data/Malta2/Results_NoiseScan/'+args.folder+'/'+args.output+'/Mask_IDB'+str(IDB_val)+'_new.txt') 
  tmp_config =  '../data/Malta2/Results_NoiseScan/'+args.folder+'/'+args.output+'/Mask_IDB'+str(IDB_val)+'_new.txt'
  #config=open(tmp_config,'a+')
  lineList = noisy_pixel.readlines()
  noisy_pixel_str = lineList#noisy_pixel.readlines()
    
  if 'chip' not in lineList[-2]:# or 'MASK' not in lineList[-1]:
    if 'chip' not in lineList[-1]:
      print ( " ... mah "  )
    else:
      chip =  re.findall(r'\b\d+\b' , lineList[-1])
      noise_rate=float(lineList[-1].split("kHz")[0].split("is:")[1])#float(chip[0])
      print ("Valerio: adding point: "+str(npixel_masked)+" -- "+str(noise_rate))
      gr_module.SetPoint(npixel_masked, npixel_masked, noise_rate)
      gr_module.SetPointError(npixel_masked, 0, ROOT.TMath.Sqrt(noise_rate / (t * r * 1e-3))) # GG
    print ("end of the scan")
    noisy_pixel.close()
    break

  chip =  re.findall(r'\b\d+\b' , lineList[-2])
  noise_rate=float(lineList[-2].split("kHz")[0].split("is:")[1])#float(chip[0])
  #  print ("VALERIO BESTEMMIA: "+str(noise_rate))
  gr_module.SetPoint(npixel_masked, npixel_masked, noise_rate)
  gr_module.SetPointError(npixel_masked, 0, ROOT.TMath.Sqrt(noise_rate / (t * r * 1e-3))) # GG

  pixel = re.search('rate '+'(\d*)', lineList[-1], re.IGNORECASE)
  #print(pixel.group(1)) 
  dublicate_check = lineList[-1][:20]
  noisest_rate = float(pixel.group(1))
  gr_pixel.SetPoint(npixel_masked, npixel_masked, noisest_rate)

  #print( "\033[1m \033[34m"+lineList[-1]+" \033[0m") 
  #if noisest_rate < limit and limit >1:
  #   limit = limit/10 
  #   noisest_rate=10000000
  #   print("\033[1m \033[92m"+"change target rate at "+str(limit)+" kHz "+"  \033[0m")
  #continue 

  # if dublicate_check in  noisy_pixel.read():
  #  if noisest_rate > 1 :
  #    print("\033[1m \033[91m"+"this pixel should be masked!!!!"+" \033[0m")
  #    print( dublicate_check )
  #    exit(0)
  #  else:
  #    break

  # prevents code from being stuck on same "unmaskable" pixel and exits before nmax is reached
  #print noisy_pixel_str
  n_occurrencies = -1
  for l in noisy_pixel_str:
    if dublicate_check in l: n_occurrencies+=1
  print (dublicate_check+"      MG: "+str(n_occurrencies))
  n_occurrencies_max = 15
  if n_occurrencies>=n_occurrencies_max:
    break

  noisy_pixel.seek(0)
  npixel_masked+=1
  noisy_pixel.close()
  #config.close()
  print (" ")
  print ("Conditions in the loop:::: Chip Noise = " + str(noise_rate)+"; N masked pixels = " + str(npixel_masked))
  print (" ")

gr_pixel.SetLineColor(2)
gr_module.SetLineColor(4)
gr_pixel.SetMarkerColor(2)
gr_module.SetMarkerColor(4)
gr_pixel.SetLineWidth(2)
gr_module.SetLineWidth(2)

canPix = ROOT.TCanvas()
gr_pixel.Draw()
canPix.SetLogy()
canPix.Print("../data/Malta2/Results_NoiseScan/"+args.folder+"/"+args.output+"/noisest_masked.pdf")

canModule = ROOT.TCanvas()
gr_module.SetMarkerColor(4)
gr_module.SetMarkerSize(1.2)
gr_module.SetMarkerStyle(20)
gr_module.Draw("APL")
canModule.SetLogy()
canModule.Print("../data/Malta2/Results_NoiseScan/"+args.folder+"/"+args.output+"/noise_masked.pdf")

file_output = ROOT.TFile("../data/Malta2/Results_NoiseScan/"+args.folder+"/"+args.output+"/noise_masked_pix.root","recreate")
file_output.cd()	
gr_pixel.Write()
gr_module.Write()
canPix.Write()
canModule.Write()
file_output.Close()
