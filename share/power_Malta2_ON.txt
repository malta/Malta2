MALTA_PSU.py -c rampVoltage SUB 1 5
MALTA_PSU.py -c rampVoltage PWELL 1 5
MALTA_PSU.py -c rampVoltage SUB 3 5
MALTA_PSU.py -c rampVoltage PWELL 3 5
MALTA_PSU.py -c rampVoltage SUB 6 5
MALTA_PSU.py -c rampVoltage PWELL 6 5
MALTA_PSU.py -c enablePSU DVDD 
MALTA_PSU.py -c enablePSU AVDD 
