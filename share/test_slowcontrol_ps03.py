#!/usr/bin/env python
import PyMalta2
import time
import signal
import argparse
import random
import sys


#Test declaration of arguments needed to run it
parser = argparse.ArgumentParser(description='Hi there')
parser.add_argument("-r","--repetitions", help='number of repetitions', type=int, default=1)
parser.add_argument("--gw", help='Gateaway router number:7 or 4"', type=int, default=7)
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
parser.add_argument("-R","--RESET",help="ONLY reset the chip",action='store_true')
args = parser.parse_args()
repetitions=args.repetitions
v=args.verbose
print("Verbose: %s" % str(v))
skip_FIFOs=False



m=PyMalta2.Malta2()
st_connect="udp://ep-ade-gw-0%i:50001" % args.gw
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
m.SetVerbose(v)

#print("m.Word()")
#print(m.Word("SET_ICASN"))

cont = True
def signal_handler(signal, frame):
    print('You pressed ctrl+C')
    global cont
    cont = False
    return

signal.signal(signal.SIGINT, signal_handler)

def test_defaults():
    # SetDefaults with true calls WriteAll method
    m.SetDefaults(True)
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # state 1 starts test
    m.SetState(1)
    time.sleep(0.1)
    # reads response from emulator
    m.ReadAll()
    time.sleep(0.1)
    # print on screen result. False because I just ReadAll()
    return m.Dump(False)

def test_changing_settings():
    #SetDefaults with false! So I will need to WriteAll()
    m.SetDefaults(False)
    for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
        m.Write(x,1,False)# put them in 1 to test
        pass
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.1)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.2)
    return m.Dump(True)

def test_changing_settings_FIFO():
    #SetDefaults with false! So I will need to WriteAll()
    m.Reset()
    m.SetDefaults(False)
    #for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
    #    m.Write(x,0,False)# put them in 1 to test
    #    pass
    #101000....00001
    #m.Write(m.Word("DATAFLOW_MERGERTOLEFT"),1,False)
    #m.Write(m.Word("DATAFLOW_MERGERTORIGHT"),0,False)
    #m.Write(m.Word("DATAFLOW_LMERGERTOLVDS"),1,False)
    m.Write(m.Word("LVDS_VBCMFB"),0xD,False)
    #
    m.Write(m.Word("PULSE_MON_L"),1,False)
    m.Write(m.Word("PULSE_MON_R"),1,False)
    m.Write(m.Word("SWCNTL_DACMONV"),1,False)
    m.Write(m.Word("SWCNTL_DACMONI"),1,False)
    #m.Write(m.Word("SET_IRESET_BIT"),1,False)
    time.sleep(0.5)
    # hot encoding
    print("--")
    #'''
    m.Write(m.Word("SET_VCASN"),110,False)
    m.Write(m.Word("SET_VCLIP"),125,False)
    m.Write(m.Word("SET_VPULSE_HIGH"),90,False)
    m.Write(m.Word("SET_VPULSE_LOW"),10,False)
    m.Write(m.Word("SET_VRESET_P"),29,False)
    m.Write(m.Word("SET_VRESET_D"),65,False)
    #'''
    #m.Write(m.Word("SET_VCASN"),0,False)
    #m.Write(m.Word("SET_VCLIP"),0,False)
    #m.Write(m.Word("SET_VPLSE_HIGH"),0,False)
    #m.Write(m.Word("SET_VPLSE_LOW"),0,False)
    #m.Write(m.Word("SET_VRESET_P"),0,False)
    #m.Write(m.Word("SET_VRESET_D"),0,False)
    '''
    # temp encoder
    '''
    m.Write(m.Word("SET_ICASN"),5,False)# put them in 1 to test
    m.Write(m.Word("SET_IRESET"),30,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),120,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),43,False)
    m.Write(m.Word("SET_IDB"),110,False)
    #m.Write(m.Word("PULSE_HOR"),0,False)
    #m.Write(m.Word("PULSE_COL"),3,False)
    #m.SetPixelPulseRow(289,True)
    m.SetPixelPulseColumn(510,True)
    #m.SetPixelPulseRow(,True)
    m.SetPixelPulseRow(291,True)
    #m.set
    '''
    m.Write(m.Word("SET_ICASN"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_IRESET"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),0,False)
    m.Write(m.Word("SET_IDB"),0,False)
    m.Write(m.Word("PULSE_HOR"),0,False)
    '''
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.02)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.02)
    #return m.DumpByWord(True)
    return m.CheckSlowControl(True)



if args.RESET==True:
    m.Reset()
else:
    total_errors=0
    for x in range(0,repetitions):
        if cont==False: break
        #total_errors+=test_defaults()
        #total_errors+=test_changing_settings()
        total_errors+=test_changing_settings_FIFO()
        #total_errors+=word101()
        pass
    print("Number of bits that don't match: %i/4321" % total_errors)
    pass

#m.Reset()
sys.exit()
