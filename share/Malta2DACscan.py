#!/usr/bin/env python
import time
import datetime
import sys
from array import array
import argparse
import os

#import MiniMaltaMap
import PyMalta2
from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree

parser=argparse.ArgumentParser()
#parser.add_argument('-r' ,'--reg'    , help='Which register to change',type=str,default='SC_VCASN')

#args=parser.parse_args()

###variable_st="SET_VCASN"
#variable_st="SET_VPULSE_LOW" #args.reg
#variable_st="SET_VPULSE_HIGH" 
#variable_st="SET_VCLIP" 
#variable_st="SET_VRESET_P" 
#variable_st="SET_VRESET_D" 

variable_st="SET_IDB" #args.reg
#variable_st="SET_IRESET" #args.reg
#variable_st="SET_ITHR" #args.reg
#variable_st="SET_IBIAS" #args.reg
#variable_st="SET_ICASN" #args.reg
#variable= globals()[variable_st]

measure="V"
if "SET_I"   in variable_st: measure="I"
elif "SET_V" in variable_st: measure="V"
else:
    print ("PLEASE GIVE ME A CORRECT VARIABLE")
    sys.exit()

path="malta2_dac_scans/"
os.system("mkdir -p "+path)

m=PyMalta2.Malta2()
st_connect="udp://ep-ade-gw-07:50001" 
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
#m.SetVerbose(v)

m.Reset()
m.SetDefaults(False)
m.Write(m.Word("PULSE_MON_L"),1,False)
m.Write(m.Word("PULSE_MON_R"),1,False)
m.Write(m.Word("SWCNTL_DACMONV"),1,False)
m.Write(m.Word("SWCNTL_DACMONI"),1,False)
m.Write(m.Word("SET_VCASN")      ,110,False)
m.Write(m.Word("SET_VCLIP")      ,126,False)
m.Write(m.Word("SET_VPULSE_HIGH"), 90,False)
m.Write(m.Word("SET_VPULSE_LOW") , 10,False)

m.Write(m.Word("SET_VRESET_P"),29 ,False)
m.Write(m.Word("SET_VRESET_D"),65 ,False)
m.Write(m.Word("SET_ICASN")   ,5  ,False)
m.Write(m.Word("SET_IRESET")  ,30 ,False)
m.Write(m.Word("SET_ITHR")    ,120,False)
m.Write(m.Word("SET_IBIAS")   ,43 ,False)
m.Write(m.Word("SET_IDB")     ,110,False)
   
m.Write(m.Word("MASK_COL"),0 ,False)
m.Write(m.Word("MASK_HOR"),0 ,False)
m.Write(m.Word("MASK_DIAG")   ,0  ,False)
m.Write(m.Word("MASK_FULLCOL")  ,0 ,False)

m.WriteAll()# needed because setdefaults is set to False
time.sleep(0.02)
# resets everything on FPGA
m.SetState(0)
time.sleep(0.01)
# start test
m.SetState(1)
time.sleep(0.02)

##################################
psuport="/dev/ttyUSB3"

psucontrol=0#"MALTA_PSU"
if psucontrol=="MALTA_PSU":
    import MALTA_PSU
    psu=MALTA_PSU.MALTA_PSU()
    psu.addPSU("monitor",psuport,"k",0,0.0,0.0)
    psu.setVoltageLimit("monitor",1.8)
    psu.setVoltage("monitor",0)
    psu.enablePSU("monitor")
    pass
else:
    import Keithley
    k=Keithley.Keithley(psuport)
    k.setVerbose(True)
    k.enableOutput(False)
    print(k.getModel())
    if "SET_I" in variable_st: ##measure=="I":
        k.setSourceVoltage()
        #k.setVoltageRange(0.001)
        #k.setVoltageLimit(1.8)
        k.setVoltage(1.8)
        #k.setVoltageRange(1e-3)
        k.setCurrentRange(1e-5)
        k.setCurrentLimit(250e-6)
        k.setCurrentHighPrecision()
        pass
    elif "SET_V" in variable_st: ##measure=="V":
        #for Voltage
        ##########################################k.setVoltage(1.8)# ALWAYS SET 1.8 TO MEASURE V
        #k.setCurrent(0)
        k.setCurrentLimit(0.25)
        k.setVoltageLimit(0)            
        pass
    pass
    k.enableOutput(True)

    

##################################################################################################


now=datetime.datetime.now()
now_st=now.strftime("%Y%m%d__%H_%M_%S")
hfile = TFile( path+'scan_'+variable_st+"_"+now_st+'.root', 'RECREATE', '' )
h_i_Volt = TH2F( 'hiVolt', 'i vs Voltage', 128, 0, 127, 40, 0, 0.2 )
h_r_Volt = TH2F( 'hrVolt', 'readout vs Voltage', 128, 0, 127, 40, 0, 0.2 )
#h_i_r = TH2F( 'hir', 'i vs readout', 127, 0, 255, 256, 0, 255 )

#start = 227
#n = 256

start = 30
n = 126

prefix = 0x100
#Values_vclip=[]
x_i, r_i, v_i = array( 'f' ), array( 'f' ), array( 'f' )
ntuple       = TTree("DAC","")
n_x_i     = array('f',(0,))     
n_r_i     = array('f',(0,))  
n_v_i     = array('f',(0,))  
ntuple.Branch("current",   n_x_i,   "current/F")
ntuple.Branch("readout",   n_r_i,   "readout/F")
ntuple.Branch("voltage",   n_v_i,   "voltage/F")
#variable=SC_ITHR


attempt=0
def doAction(target):
    global attempt
    attempt+=1
    print ("Attempt: "+str(attempt))
    mm.SetDefaults()
    mm.Write(variable, target)
    mm.Send()
    time.sleep(0.001)
    mm.Recv()
    #readOut = mm.Read(variable)
    #if attempt==100: return True
    #if readOut!=target: 
    #    print "readOut: %s [%s]. Target %s [%s]" % (str(readOut), str(bin(readOut)), str(target), str(bin(target)))
    #    return False
    #else : 
    return True




count=start
for i in range(start,n):
    print ("Attempt: "+str(i))
    attempt=0
    time.sleep(0.01)
    m.Write(m.Word(variable_st), i,False)
    m.WriteAll()
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.02)
    # start test
    m.SetState(1)
    time.sleep(0.01)

    #mm.Write(variable, prefix+i)
    #mm.Write(SC_VPULSE_H, prefix+i)
    #mm.Send()
    #time.sleep(0.1)
    #mm.Recv()
    #readOut = mm.Read(variable)
    #readOut = mm.Read(SC_VPULSE_H)
    
    #res=doAction(prefix+i)
    #while res==False:
    #    res=doAction(prefix+i)
    #    pass
    readOut = i #mm.Read(variable)
    #time.sleep(0.01)

    #debug = mm.Read(SC_VCLIP)
    #time.sleep(1.)
    #mean = 0.0
    #for j in range(0,5):
    #    time.sleep(0.3)
    #    mean = mean + k.getVoltage()
    tval=0
    reps=3
    goods=0
    time.sleep(1)
    if psucontrol=="MALTA_PSU":
        val=psu.getVoltage("monitor") if measure=="V" else psu.getCurrent(0)
        pass
    else:
        for x in range(0,reps):
            if measure=="I":
                #k.setCurrentRange(1e-3)
                #k.setCurrentHighPrecision() 
                tmp= 1##k.getCurrent() 
                print("[%i]\ttmp: %s" %(x , str(tmp)))
                if tmp<90 and tmp > 0:
                    tval+=tmp
                    goods+=1
                pass
            elif measure=="V":
                val=0
                tmp = k.getVoltage()
                print("[%i]\ttmp: %s" %(x , str(tmp)))
                if tmp<90 and tmp > 0:
                    tval+=tmp
                    goods+=1
                pass
            pass
            ## VD: resending config ....
            #m.SetState(0)
            #time.sleep(0.01)
            ## start test
            #m.SetState(1)
            #time.sleep(0.02)
        pass
    val=tval/(goods)
    print("tval:%s\t goods:%s\t val:%s" % (str(tval),str(goods),str(val)))
    
    #h_r_Volt.Fill(readOut, val)
    #h_i_r.Fill(i, readOut)
    #print i, type(i)
    count+=1
    val=round(val, 8)
    if count==(start+1) or val==-999 or val == 999: continue
    if len(v_i)>0:
        print ("input value  : " + "dec: " + str(i) + "  bin: " + bin(prefix+i) + "; readOut: " + str(readOut) + " = " + bin(readOut) + "; "+measure+" = " + str(val)+" diff to prev: "+str(round(val-v_i[-1],8)))
    else:
        print ("input value  : " + "dec: " + str(i) + "  bin: " + bin(prefix+i) + "; readOut: " + str(readOut) + " = " + bin(readOut) + "; "+measure+" = " + str(val))
    h_i_Volt.Fill(i, val)
    x_i.append(i)
    #r_i.append(readOut)
    v_i.append(val)
    n_x_i[0]=i
    #n_r_i[0]=readOut
    n_v_i[0]=val
    
    pass

if psucontrol=="MALTA_PSU":
    psu.disablePSU("monitor")
else:
    k.enableOutput(False)

print (x_i)
print (v_i)

#gr_i_v = TGraph( n-start, x_i, v_i )
gr_i_v = TGraph( len(x_i), x_i, v_i )
gr_i_v.SetMarkerStyle( 21 )
gr_i_v.GetXaxis().SetTitle( 'inputDAC' )
gr_i_v.GetYaxis().SetTitle( measure +" "+variable_st)
gr_i_v.Write( "graph_input_vs_measure" )

#gr_r_v = TGraph( n-start, r_i, v_i )
#gr_r_v.SetMarkerStyle( 23 )
#gr_r_v.GetXaxis().SetTitle( 'outputDAC' )
#gr_r_v.GetYaxis().SetTitle( measure +" "+variable_st)
#gr_r_v.Write( "graph_output_vs_measure")

#gr_i_r = TGraph( n-start, x_i, r_i )
#gr_i_r.SetMarkerStyle( 21 )
#gr_i_r.GetXaxis().SetTitle( 'inputDAC' )
#gr_i_r.GetYaxis().SetTitle( 'outputDAC' )
#gr_i_r.Draw( 'AP' )
#gr_i_r.Write( "graph_input_vs_output" )

ntuple.Fill()
hfile.Write()

c1 = TCanvas( 'c1', 'E', 200, 10, 700, 500 )
gr_i_v.Draw("APL")
c1.Print(path+'scan_'+variable_st+"_"+now_st+'.pdf')

# To hold canvas window open 
#text = input()

#print ("The voltages read are: ", v_i)
print ("The currents read are: ", v_i)

print ("Done")
sys.exit()

