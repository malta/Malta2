/************************************
 * Malta2
 * Brief: Python module for Malta2
 * 
 * Author: Carlos.Solans@cern.ch
 *         Ignacio.Asensi@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "Malta2/Malta2.h"
#include <iostream>
#include <vector>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif



#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  static struct PyModuleDef moduledef = {\
		     PyModuleDef_HEAD_INIT, name, doc,-1, methods\
  };\
  m = PyModule_Create(&moduledef);\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)\
  ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000



/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  Malta2 *obj;
  std::vector<uint32_t> vec;
} PyMalta2;

static int _PyMalta2_init(PyMalta2 *self)
{

    self->obj = new Malta2();
    return 0;
}

static void _PyMalta2_dealloc(PyMalta2 *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}


PyObject * _PyMalta2_SetVerbose(PyMalta2 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    std::cout << "ERROR IN SetVerbose py module" << std::endl;
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetVerbose(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetIPbus(PyMalta2 *self, PyObject *args)
{
    /*
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    self->obj->SetIPbus(pos);
    */
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_GetIPbus(PyMalta2 *self)
{
  //ipbus::Uhal* ipb = self->obj->GetIPbus();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_Connect(PyMalta2 *self, PyObject *args)
{
    char * str;
    if(PyArg_ParseTuple(args, (char *) "s",&str)){
      self->obj->Connect(std::string(str));
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_SetDefaults(PyMalta2 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool val = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  
    self->obj->SetDefaults(val);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_GetDefault(PyMalta2 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    PyObject *py_ret;
    uint32_t ret = self->obj->GetDefault(pos);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMalta2_Write(PyMalta2 *self, PyObject *args)
{
    uint32_t pos;
    uint32_t val;
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "IIO",&pos, &val, &py_enable)){
      std::cout << " PyMalta2 Error in number of parameters!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      Py_INCREF(Py_None);
      return Py_None;
    }
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    self->obj->Write(pos, val, enable);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_WriteAll(PyMalta2 *self)
{
      self->obj->WriteAll();
      Py_INCREF(Py_None);
      return Py_None;
}

PyObject * _PyMalta2_Reset(PyMalta2 *self)
{
      self->obj->Reset();
      Py_INCREF(Py_None);
      return Py_None;
}

PyObject * _PyMalta2_Read(PyMalta2 *self, PyObject *args)
{
    uint32_t pos;
    PyObject *py_enable = NULL;
    if (!PyArg_ParseTuple(args, (char *) "IO",&pos, &py_enable)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    
    bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
    PyObject *py_ret;
    uint32_t ret = self->obj->Read(pos, enable);
    py_ret = PyLong_FromLong(ret);
    return py_ret;
}

PyObject * _PyMalta2_ReadAll(PyMalta2 *self)
{
    self->obj->ReadAll();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_Configure(PyMalta2 *self)
{
    self->obj->Configure();
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_DumpByWord(PyMalta2 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->DumpByWord(update);
  py_ret = PyLong_FromLong(ret);
  return py_ret;  
}

PyObject * _PyMalta2_CheckSlowControl(PyMalta2 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->CheckSlowControl(update);
  py_ret = PyLong_FromLong(ret);
  return py_ret;  
}

PyObject * _PyMalta2_DumpByIPbus(PyMalta2 *self, PyObject *args)
{
  PyObject *py_update = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_update)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool update = py_update? (bool) PyObject_IsTrue(py_update) : false;
  PyObject *py_ret;
  uint32_t ret = self->obj->DumpByIPbus(update);
  py_ret = PyLong_FromLong(ret); 
  return py_ret;  
}


PyObject * _PyMalta2_SetState(PyMalta2 *self, PyObject *args)
{
    uint32_t pos;
    if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
      Py_INCREF(Py_None);
      return Py_None;
    }
    
    self->obj->SetState(pos);
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject * _PyMalta2_Word(PyMalta2 *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  PyObject *py_ret;
  uint32_t ret=self->obj->Word(std::string(str));
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMalta2_Send(PyMalta2 *self)
{   
  self->obj->Send();
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMalta2_PrintFullSCWord(PyMalta2 *self)
{   
  self->obj->PrintFullSCWord();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPulseWidth(PyMalta2 *self, PyObject *args)
{
  uint32_t width;
  if(PyArg_ParseTuple(args, (char *) "I",&width)){
    self->obj->SetPulseWidth(width);
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ReadFifoStatus(PyMalta2 *self)
{   
  self->obj->ReadFifoStatus();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_IsFifo1Full(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Full()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_IsFifo1Empty(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Empty()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_IsFifo1Half(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo1Half()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_IsFifo2Full(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Full()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_IsFifo2Empty(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Empty()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_IsFifo2Half(PyMalta2 *self)
{ 
  PyObject * ret = Py_True;
  if(!self->obj->IsFifo2Half()){ ret = Py_False; }
  Py_INCREF(ret);
  return ret;
}

PyObject * _PyMalta2_WriteConstDelays(PyMalta2 *self, PyObject *args)
{
  uint32_t delay1, delay2;
  if (PyArg_ParseTuple(args, (char *) "II",&delay1,&delay2)){
    self->obj->WriteConstDelays(delay1, delay2);
  }    
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_WriteTap(PyMalta2 *self, PyObject *args)
{
  uint32_t bit, tap1, tap2;
  if (PyArg_ParseTuple(args, (char *) "III",&bit,&tap1,&tap2)){
    self->obj->WriteTap(bit,tap1,tap2);
  }    
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ReadTap(PyMalta2 *self, PyObject *args)
{
  uint32_t bit;
  if (!PyArg_ParseTuple(args, (char *) "I",&bit)){
    Py_INCREF(Py_None);
    return Py_None;
  }    
  std::vector<uint32_t> ret = self->obj->ReadTap(bit);
  PyObject * py_ret = PyList_New(ret.size()); 
  for(uint32_t i=0;i<ret.size();i++){
    PyObject * it = PyLong_FromLong(ret.at(i));
    if(PyList_SetItem(py_ret,i,it)!=0){
      Py_INCREF(Py_None);
      return(Py_None);
    }
  }
  return py_ret;
}


PyObject * _PyMalta2_WriteTapsToFile(PyMalta2 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->ReadTapsFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ReadTapsFromFile(PyMalta2 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->ReadTapsFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_Trigger(PyMalta2 *self, PyObject *args)
{
  uint32_t ntimes;
  PyObject *py_withPulse = NULL;
  if(PyArg_ParseTuple(args, (char *) "|IO",&ntimes,&py_withPulse)){
    bool withPulse = py_withPulse? (bool) PyObject_IsTrue(py_withPulse) : false;
    self->obj->Trigger(ntimes,withPulse);
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetReadoutDelay(PyMalta2 *self, PyObject *args)
{
  uint32_t delay;
  if (PyArg_ParseTuple(args, (char *) "I",&delay)){
    self->obj->SetReadoutDelay(delay); 
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_GetReadoutDelay(PyMalta2 *self)
{ 
  return PyLong_FromLong(self->obj->GetReadoutDelay()); 
}

PyObject * _PyMalta2_SetPixelPulse(PyMalta2 *self, PyObject *args)
{
  uint32_t row,col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&row,&col,&py_enable)){
    self->obj->SetPixelPulse(row,col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelPulseRow(PyMalta2 *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    self->obj->SetPixelPulseRow(row,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelPulseColumn(PyMalta2 *self, PyObject *args)
{
  uint32_t col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&col,&py_enable)){
    self->obj->SetPixelPulseColumn(col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelMask(PyMalta2 *self, PyObject *args)
{
  uint32_t row,col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&row,&col,&py_enable)){
    self->obj->SetPixelMask(row,col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelMaskRow(PyMalta2 *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    self->obj->SetPixelMaskRow(row,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelMaskColumn(PyMalta2 *self, PyObject *args)
{
  uint32_t col;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&col,&py_enable)){
    self->obj->SetPixelMaskColumn(col,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetPixelMaskDiag(PyMalta2 *self, PyObject *args)
{
  uint32_t diag;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&diag,&py_enable)){
    self->obj->SetPixelMaskDiag(diag,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetDoubleColumnMask(PyMalta2 *self, PyObject *args)
{
  uint32_t dc;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IO",&dc,&py_enable)){
    self->obj->SetDoubleColumnMask(dc,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetDoubleColumnMaskRange(PyMalta2 *self, PyObject *args)
{
  uint32_t dc1,dc2;
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIO",&dc1,&dc2,&py_enable)){
    self->obj->SetDoubleColumnMaskRange(dc1,dc2,(py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetROI(PyMalta2 *self, PyObject *args)
{
  uint32_t col1,row1,col2,row2;
  PyObject *py_mask = NULL;
  if(PyArg_ParseTuple(args, (char *) "IIIIO",&col1,&row1,&col2,&row2,&py_mask)){
    self->obj->SetROI(col1,row1,col2,row2,(py_mask? (bool) PyObject_IsTrue(py_mask) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetFullPixelMaskFromFile(PyMalta2 *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->SetFullPixelMaskFromFile(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetExternalL1A(PyMalta2 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    self->obj->SetExternalL1A((py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ReadoutOn(PyMalta2 *self)
{
  self->obj->ReadoutOn();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ReadoutOff(PyMalta2 *self)
{
  self->obj->ReadoutOff();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_EnableFastSignal(PyMalta2 *self)
{
  self->obj->EnableFastSignal();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_DisableFastSignal(PyMalta2 *self)
{
  self->obj->DisableFastSignal();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_SetFastSignal(PyMalta2 *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if(PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    self->obj->SetFastSignal((py_enable? (bool) PyObject_IsTrue(py_enable) : false));
  }  
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_ResetL1Counter(PyMalta2 *self)
{
  self->obj->ResetL1Counter();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMalta2_GetL1ID(PyMalta2 *self)
{
  return PyLong_FromLong(self->obj->GetL1ID()); 
}

PyObject * _PyMalta2_GetFIFO2WordCount(PyMalta2 *self)
{
  return PyLong_FromLong(self->obj->GetFIFO2WordCount()); 
}

////////////////////////////////////////////////////////////////


static PyMethodDef PyMalta2_methods[] = {
  {(char *) "SetVerbose",(PyCFunction) _PyMalta2_SetVerbose, METH_VARARGS, NULL },
  {(char *) "SetIPbus",(PyCFunction) _PyMalta2_SetIPbus, METH_VARARGS, NULL },
  {(char *) "GetIPbus",(PyCFunction) _PyMalta2_GetIPbus, METH_NOARGS, NULL },
  {(char *) "Connect",(PyCFunction) _PyMalta2_Connect, METH_VARARGS, NULL },
  {(char *) "SetDefaults",(PyCFunction) _PyMalta2_SetDefaults, METH_VARARGS, NULL },
  {(char *) "GetDefault",(PyCFunction) _PyMalta2_GetDefault, METH_VARARGS, NULL },
  {(char *) "Write",(PyCFunction) _PyMalta2_Write, METH_VARARGS, NULL },
  {(char *) "WriteAll",(PyCFunction) _PyMalta2_WriteAll, METH_NOARGS, NULL },
  {(char *) "Reset",(PyCFunction) _PyMalta2_Reset, METH_NOARGS, NULL },
  {(char *) "Read",(PyCFunction) _PyMalta2_Read, METH_VARARGS, NULL },
  {(char *) "ReadAll",(PyCFunction) _PyMalta2_ReadAll, METH_NOARGS, NULL },
  {(char *) "Configure",(PyCFunction)   _PyMalta2_Configure, METH_VARARGS, NULL },
  {(char *) "SetState",(PyCFunction)   _PyMalta2_SetState, METH_VARARGS, NULL },
  {(char *) "DumpByWord",(PyCFunction) _PyMalta2_DumpByWord, METH_VARARGS, NULL },
  {(char *) "CheckSlowControl",(PyCFunction) _PyMalta2_CheckSlowControl, METH_VARARGS, NULL },
  {(char *) "DumpByIPbus",(PyCFunction) _PyMalta2_DumpByIPbus, METH_VARARGS, NULL },
  {(char *) "Word",(PyCFunction) _PyMalta2_Word, METH_VARARGS, NULL },
  {(char *) "Send",(PyCFunction) _PyMalta2_Send, METH_NOARGS, NULL },
  {(char *) "PrintFullSCWord",(PyCFunction) _PyMalta2_PrintFullSCWord, METH_NOARGS, NULL },
  {(char *) "SetPulseWidth",(PyCFunction) _PyMalta2_SetPulseWidth, METH_VARARGS, NULL },
  {(char *) "ReadFifoStatus",(PyCFunction) _PyMalta2_ReadFifoStatus, METH_NOARGS, NULL },
  {(char *) "IsFifo1Full",(PyCFunction) _PyMalta2_IsFifo1Full, METH_NOARGS, NULL },
  {(char *) "IsFifo1Empty",(PyCFunction) _PyMalta2_IsFifo1Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo1Half",(PyCFunction) _PyMalta2_IsFifo1Half, METH_NOARGS, NULL },
  {(char *) "IsFifo2Full",(PyCFunction) _PyMalta2_IsFifo2Full, METH_NOARGS, NULL },
  {(char *) "IsFifo2Empty",(PyCFunction) _PyMalta2_IsFifo2Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo2Half",(PyCFunction) _PyMalta2_IsFifo2Half, METH_NOARGS, NULL },
  {(char *) "WriteConstDelays",(PyCFunction) _PyMalta2_WriteConstDelays, METH_VARARGS, NULL },
  {(char *) "ReadTap",(PyCFunction) _PyMalta2_ReadTap, METH_VARARGS, NULL },
  {(char *) "WriteTap",(PyCFunction) _PyMalta2_WriteTap, METH_VARARGS, NULL },
  {(char *) "ReadTapsFromFile",(PyCFunction) _PyMalta2_ReadTapsFromFile, METH_VARARGS, NULL },
  {(char *) "WriteTapsToFile",(PyCFunction) _PyMalta2_WriteTapsToFile, METH_VARARGS, NULL },
  {(char *) "Trigger",(PyCFunction) _PyMalta2_Trigger, METH_VARARGS, NULL },
  {(char *) "SetReadoutDelay",(PyCFunction) _PyMalta2_SetReadoutDelay, METH_VARARGS, NULL },
  {(char *) "GetReadoutDelay",(PyCFunction) _PyMalta2_GetReadoutDelay, METH_NOARGS, NULL },   
  {(char *) "SetPixelPulse",(PyCFunction) _PyMalta2_SetPixelPulse, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseRow",(PyCFunction) _PyMalta2_SetPixelPulseRow, METH_VARARGS, NULL },
  {(char *) "SetPixelMask",(PyCFunction) _PyMalta2_SetPixelMask, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskRow",(PyCFunction) _PyMalta2_SetPixelMaskRow, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskColumn",(PyCFunction) _PyMalta2_SetPixelMaskColumn, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskDiag",(PyCFunction) _PyMalta2_SetPixelMaskDiag, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseColumn",(PyCFunction) _PyMalta2_SetPixelPulseColumn, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMask",(PyCFunction) _PyMalta2_SetDoubleColumnMask, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMaskRange",(PyCFunction) _PyMalta2_SetDoubleColumnMaskRange, METH_VARARGS, NULL },
  {(char *) "SetFullPixelMaskFromFile",(PyCFunction) _PyMalta2_SetFullPixelMaskFromFile, METH_VARARGS, NULL },
  {(char *) "SetROI",(PyCFunction) _PyMalta2_SetROI, METH_VARARGS, NULL },
  {(char *) "SetExternalL1A",(PyCFunction) _PyMalta2_SetExternalL1A, METH_VARARGS, NULL },
  {(char *) "ReadoutOn",(PyCFunction) _PyMalta2_ReadoutOn, METH_NOARGS, NULL },   
  {(char *) "ReadoutOff",(PyCFunction) _PyMalta2_ReadoutOff, METH_NOARGS, NULL },   
  {(char *) "EnableFastSignal",(PyCFunction) _PyMalta2_EnableFastSignal, METH_NOARGS, NULL },   
  {(char *) "DisableFastSignal",(PyCFunction) _PyMalta2_DisableFastSignal, METH_NOARGS, NULL },   
  {(char *) "ResetL1Counter",(PyCFunction) _PyMalta2_ResetL1Counter, METH_NOARGS, NULL },   
  {(char *) "GetL1ID",(PyCFunction) _PyMalta2_GetL1ID, METH_NOARGS, NULL },   
  {(char *) "GetFIFO2WordCount",(PyCFunction) _PyMalta2_GetFIFO2WordCount, METH_NOARGS, NULL },   
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyMalta2_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMalta2.Malta2",            /* tp_name */
    sizeof(PyMalta2),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMalta2_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    0,                                   /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMalta2_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMalta2_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
    ,0                               /* tp_version_tag */
#endif
};

static PyMethodDef module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMalta2)
{
  PyObject *m;
  MOD_DEF(m, "PyMalta2", NULL, module_methods);
  if(PyType_Ready(&PyMalta2_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "Malta2", (PyObject *) &PyMalta2_Type);
  return MOD_RETURN(m);
}
