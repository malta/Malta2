#include "Malta2/Malta2Module.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <bitset>
#include "Malta2/Malta2Utils.h"

#include "TH2D.h"
DECLARE_READOUTMODULE(Malta2Module)
using namespace std;

Malta2Module::Malta2Module(string name, string address, string outdir): 
  ReadoutModule(name,address,outdir){  
  
  m_malta2 = new Malta2();
  m_malta2->Connect(address);
  m_ntuple = 0;
  ostringstream os;
  os << m_name << "_timing";
  m_Htime=new TH1D(os.str().c_str(),"timing;time since L1A [ns]",160,0,500); 
  os.str("");
  os << m_name << "_nPixel";
  m_Hsize=new TH1D(os.str().c_str(),"pixel;# pixel per event",6,-0.5,5.5); 
  os.str("");
  os << m_name << "_hitMap";
  ostringstream ost;
  ost << name << ";pix X; pix Y";
  //m_Hmap =new TH2D(os.str().c_str(),ost.str().c_str(),512,0,512,224,228,512);
  m_Hmap =new TH2D(os.str().c_str(),ost.str().c_str(),512,0,512,512,0,512);
  
  m_internalTrigger=false;
/*
  ostringstream osp;
  osp << m_name << "_pX";
  m_pX=new TH1F(osp.str().c_str(),";hit posX [mm]",50,-256*0.0368,+256*0.0368);
  ostringstream osp2;
  osp2 << m_name << "_pY";
  m_pY=new TH1F(osp2.str().c_str(),";hit posY [mm]",50,-256*0.0368,+256*0.0368);
*/

}

Malta2Module::~Malta2Module(){
  delete m_malta2;
  if(m_ntuple) delete m_ntuple;
}

void Malta2Module::Configure(){
  
  m_malta2->SetVerbose(m_verbose);

  ReadoutConfig * config=GetConfig();
  
  m_malta2->Reset(); 
  //m_malta2->SetSCClock(true);
  m_malta2->ResetFifo();
  m_malta2->SetDefaults(false);
  Malta2Utils::EnableAll(m_malta2);

  /*   
  //Default for all MALTA versions
  m_malta2->EnableMerger(false);//false
  m_malta2->SetPulseWidth(750); //500 from 11-03-2020
  cout << "before pulse" << endl;
  //Default config for MALTA_C
  m_malta2->EnableMonDacCurrent(false);//VD we should really put False 11-03-2020
  m_malta2->EnableMonDacVoltage(false); //VD we should really put False 11-03-2020
  m_malta2->EnableIDB(false);
  m_malta2->EnableITHR(false);
  m_malta2->EnableIRESET(false);
  m_malta2->EnableICASN(false);
  m_malta2->EnableVCASN(false);
  m_malta2->EnableVCLIP(false);
  m_malta2->EnableVPULSE_HIGH(false);
  m_malta2->EnableVPULSE_LOW(false);
  m_malta2->EnableVRESET_P(false);
  m_malta2->EnableVRESET_D(false);
  */
 
  //cout <<"AFTER MY SEND!!!!" << endl;
  bool tap_from_file = false;
  if(config){
    for(uint32_t i=0;i<config->GetKeys().size();i++){
      string key=config->GetKeys().at(i);
      cout << key << endl;
      if (key.find("TAP")!= string::npos) tap_from_file = true; 
      m_malta2->SetConfigFromString(key);
    }
  }
  ///Milou write here!!! 
  m_malta2->SetPulseWidth(500);
  Malta2Utils::MaltaSend(m_malta2,1500,1500);
 
  //m_malta2->SetSCClock(false);
  
  if ((tap_from_file) == false) {m_malta2->WriteConstDelays(4,1);}
  //m_malta2->WriteConstDelays(3,1);
  m_malta2->ResetFifo();
  m_malta2->SetExternalL1A(true);
  m_malta2->ResetL1Counter();
  m_malta2->ReadoutOn();
  //exit(0);
  cout << "=================================================================================" << endl;
}

void Malta2Module::Start(string run, bool usePath){
  string fname;
  cout << m_name << ": Start run " << run << endl;
  if(!usePath){
    ostringstream os;
    os << m_outdir << "/" << "run_" << run << ".root";
    fname = os.str();
  }
  else{
    fname = run;
  }
  
    
  //Create ntuple
  cout << m_name << ": Create ntuple" << endl;
  if(m_ntuple) delete m_ntuple;
  m_ntuple = new Malta2Tree();
  m_ntuple->Open(fname,"RECREATE");
  cout << m_name << ": Prepare for run"<< endl;
  m_malta2->ReadoutOff();
  m_malta2->ResetL1Counter();
  m_malta2->ResetFifo();
  m_malta2->SetReadoutDelay(80);//was35
  m_malta2->ReadoutOn();
  
  cout << m_name << ": Create data acquisition thread" << endl;
  m_thread = thread(&Malta2Module::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;


  //write config Tree to ntuple
  std::map<std::string,std::string> configFromModule = GetConfig()->ConfigForROOTFile();
  std::map<std::string,std::array<char,2000>> configForROOTFile;
  m_ntuple->GetFile()->cd();

  //loop through config entries and make branch for each one, then set values for the branch
  //fill tree after loop, every branch should only have one entry
  //ignore masks because this will be written to separate histograms for pixels and double columns
  cout << "Write run info to file" << endl;
  
  TTree* info = new TTree("INFO","Info about run parameters");
  for(auto entry: configFromModule){
    if(entry.second.size() < 1999){
      info->Branch(entry.first.c_str(),&configForROOTFile[entry.first],(entry.first+"/C").c_str());
      strcpy(configForROOTFile[entry.first].data(), entry.second.c_str());
    } else {
      cout << "Char array too large: " << endl
           << entry.second << endl
           << endl << "Entry will be ignored!" << endl;
      continue;
    }
  }
  info->Fill();
  info->Write();

  //masked double columns
  m_maskDColHist = m_malta2->GetDColMask(); 
  m_maskDColHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));
  //masked pixels
  m_maskedPixelHist    = (TH2I*)m_malta2->GetPixelMask();
  //  m_maskedPixelHist->SetName("MaskedPix");
  //  m_maskedPixelHist->SetTitle("Masked pixels with no ghosts;Pix X;Pix Y; Mask Layers");
  m_maskedPixelHist->SetMaximum(2);
  m_maskedPixelHist->GetYaxis()->SetRangeUser(287.5,511.5);
  m_maskedPixelHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));
  //masked pixels+ghosts
  m_maskedPixelGhostsHist = (TH2I*)m_malta2->GetMaskedPixels(0, 0, 512, 512);
  //  m_maskedPixelGhostsHist->SetName("MaskedPixGhosts");
  //  m_maskedPixelGhostsHist->SetTitle("Masked pixels with ghosts;Pix X;Pix Y; Mask Layers");
  m_maskedPixelGhostsHist->SetMaximum(2);
  m_maskedPixelGhostsHist->GetYaxis()->SetRangeUser(287.5,511.5);
  m_maskedPixelGhostsHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));

  m_maskDColHist->Write();
  m_maskedPixelHist->Write();
  m_maskedPixelGhostsHist->Write();
  m_ntuple->GetFile()->Write();
  
}

void Malta2Module::Stop(){
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  cout << m_name << ": Join data acquisition thread" << endl;
  DisableFastSignal();        //to be removed from here
  m_malta2->ReadoutOff();
  m_malta2->ResetL1Counter();
  m_thread.join();
  m_running=false;
  //m_malta2->ResetFifo();
  cout << m_name << ": Close tree" << endl;
  m_ntuple->GetFile()->WriteObject(m_Htime,"Timing");
  m_ntuple->GetFile()->WriteObject(m_Hsize,"NHit"  );
  m_ntuple->GetFile()->WriteObject(m_Hmap ,"HitMap");
  m_ntuple->GetFile()->WriteObject(m_maskDColHist ,"dcolmask");
  m_ntuple->GetFile()->WriteObject(m_maskedPixelHist ,"pixelmask");
  m_ntuple->GetFile()->WriteObject(m_maskedPixelGhostsHist ,"maskedPixels");
  m_ntuple->Close();
}

void Malta2Module::Run(){
  //uint32_t wordsDebug[50];   

  uint32_t words[20000];       //was 2000 //putting it to 250 will restore eerything
  uint32_t numwordsMAX=20000;  //putting it to 250 will restore eerything
  uint32_t numwords=250;
  uint32_t numwordsCurrent=0;
  for (uint32_t i=0;i<numwords;i+=2) {
    words[i+0]=0;
    words[i+1]=0;
  }
  Malta2Data md,md2;


  bool isFirstTEST=true;
  bool isFirst=true;
  uint32_t phase1, phase2, winid1, winid2, l1id1, l1id2, bcid1, bcid2;
  uint32_t lastL1id=0;
  //uint32_t extL1id=0;
  m_extL1id=0;
  uint32_t prevL1id=123456;
  uint32_t lastCount=0;
  uint32_t overflow=0;
  bool markDuplicate;
  uint32_t coutCounter=0;
  
  uint32_t C_TOT =0;
  uint32_t C_WOW =0;
  uint32_t C_PROB=0;
  uint32_t C_REF0=0;
  uint32_t C_JUMP=0;
  
  uint32_t readAttempt=0;
  bool print=false;
  int nHit=0;
  
  m_cont=true;

  int countV=0;
  std::vector<std::pair<uint32_t,uint32_t>> event;
   
  bool wasEmpty=true;
  while(m_cont){
    //std::this_thread::sleep_for(1s);
  
    if (m_resetL1Counters) {
      m_malta2->ReadoutOff();
      usleep(1000); // MLB
      m_malta2->ResetL1Counter();
      //m_malta->ResetFifo();
       usleep(1000); // MLB
      m_malta2->ReadoutOn();
      //prevL1id=4096;      
      m_resetL1Counters=false;
      cout<<"reset L1 counters from socket command" <<endl;
    }

    //m_malta2->Sync();
    //m_malta2->ReadFifoStatus();
    numwordsCurrent = m_malta2->GetFIFO2WordCount();
    if (numwordsCurrent>2000 or wasEmpty) m_malta2->ReadFifoStatus();
    if(numwordsCurrent >= 2)              numwordsCurrent -= 2;
    //cout << "FIFO2 word count: " << numwordsCurrent << endl;
    if(numwordsCurrent > numwordsMAX) numwordsCurrent = numwordsMAX;
    if(numwordsCurrent > 0) wasEmpty=false;
    bool fifo2Empty = m_malta2->IsFifo2Empty();
    bool fifo1Full  = m_malta2->IsFifo1Full();
    bool fifo2Full  = m_malta2->IsFifo2Full();
    
    //cout << "FIFO2 word count: " << numwordsCurrent << " ( " << m_malta2->GetL1ID() << " ) " 
    //	 << " : [ " << fifo2Empty << " , " << fifo1Full << " , " << fifo2Full << " ]" << endl;
    if (fifo2Empty || (numwordsCurrent<4) ){
      coutCounter++;
      if (coutCounter%20==0 && false) {
        cout << m_name << ": Number of events since last EmptyFifo (" << coutCounter << "): " << lastCount 
             << "   and last L1id is: " << lastL1id
             << " (" << overflow << ") --> cumulative: " << m_extL1id
             << endl;
      }
      
      lastCount=0;
      std::this_thread::sleep_for(0.01s);
      for (uint32_t i=0;i<numwordsMAX;i+=2) {
        words[i+0]=0;
        words[i+1]=0;
      }
      wasEmpty=true;

      /*
      if ( m_maxEvt!=-1) {
        if ( m_extL1id>m_maxEvt or C_TOT>m_maxEvt ) {           
          m_cont=false;
	  m_running=false;
        }
      }
      */

      //generate some random L1a (note that the IPBus can't generate L1A which are short enough)
      if (m_internalTrigger) {
        //m_malta2->Trigger(0,false);
      }
      continue;
    }
    if(fifo1Full){
      cout << m_name << " :: FIFO1 FULL : AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << endl;
    }
    if(fifo2Full){
      cout << m_name << " :: FIFO2 FULL : OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
    }
    
    if(m_debug) cout << m_name << " Before READ m_cont=" << m_cont << endl; 

    if (numwordsCurrent<=numwords) m_malta2->ReadMaltaWord(words,numwordsCurrent);
    else  { //and here is where the agic happens
      int nTries=numwordsCurrent/numwords;
      int rest  =numwordsCurrent%numwords;
      for (int iT=0; iT<nTries; iT++) {
	m_malta2->ReadMaltaWord(&words[numwords*iT],numwords);
      }
      m_malta2->ReadMaltaWord(&words[numwords*nTries],rest);
    }
    countV++;

    if(m_debug) cout << m_name << " After READ m_cont=" << m_cont << endl; 

    // this is maybe outdated atm....
    uint32_t goodW =0;
    int NoRef =0;
    int emptyW=0;
    int badW  =0;
    //VD experimental
    for (uint32_t i=0;i<numwordsCurrent;i+=2) {
      if ( words[i+0]!=0 and words[i+1]!=0 )   {
        if ( words[i]%2!=0) goodW++;
        else                NoRef++;
      }
      else  if ( (words[i+0]+words[i+1])==0 )  emptyW++;
      else                                     badW++;
    }
    if (goodW!=(numwordsCurrent/2) ) cout << m_name << " :: PAIRS summary: " << goodW << " / " << NoRef << "/ " << emptyW << " / " << badW << endl;
    
    readAttempt++;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(m_debug) cout << m_name << " Before processing m_cont=" << m_cont << endl; 

    for(uint32_t i=0;i<numwordsCurrent;i+=2){
      
      if( m_maxEvt>0 ) { 
        //if( (int)m_extL1id>m_maxEvt) Stop();
      }
      
      if(lastCount%1000000==0 && lastCount>0){
        cout << m_name << " :: Events so far: " << lastCount << endl;
      } 
      
      m_extL1id = (lastL1id+overflow*4096);

      if(words[i+0]==0 or words[i+1]==0){
        cout << m_name << ": FUCK ME ONE OF THE WORDS IS EMPTY: " << words[i+0] << " / " << words[i+1] << endl;
        if ( (words[i+0]+words[i+1])!=0 ) {
          if ( words[i+0]==0 ) print=true;
          if ( words[i+1]==0 ) print=false;
          cout << m_name << " :: WOW " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
          C_WOW+=1;
        } else {
          if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: EMPTY " << endl;
        }
        continue;
      } else {
        if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
      }
      C_TOT++;
      
      if (words[i+0]%2==0) {
	cout << "EMPTY REF BIT!" << endl;
        C_REF0++;
        continue;
      }

      lastCount++;
      if(isFirst){
        md.setWord1(words[i+0]);
        md.setWord2(words[i+1]);
        isFirst = false;
        continue;
      }
      
      //md.dump();

      md2.setWord1(md.getWord1());
      md2.setWord2(md.getWord2());
      md.setWord1(words[i+0]);
      md.setWord2(words[i+1]);
      
      md.unpack();
      md2.unpack();
      
      m_ntuple->Set(&md2);
      if (isFirstTEST) {
        isFirstTEST=false;
        m_extL1id=md2.getL1id();
      }
      
      m_ntuple->SetL1idC(m_extL1id);
      
      bcid1  = md2.getBcid();
      phase1 = md2.getPhase();
      winid1 = md2.getWinid();
      l1id1  = md2.getL1id();
      
      bcid2  = md.getBcid();
      phase2 = md.getPhase();
      winid2 = md.getWinid();
      l1id2  = md.getL1id();

      // if (md.getPixel()==0) cout << m_name  << "########################### Pixels of event " << l1id2 << ": " << std::bitset<32>(md.getPixel()) << " " << md.getGroup() << " " << md.getDcolumn() << endl;
      //if(abs((long int)l1id1-l1id2) > 1) cout << "########################### L1ID jump from " << l1id1 << " to " << l1id2 << endl;

      int del=md.getDelay();
      int cID=md.getChipid();
      if (del!=0 or cID!=0 or bcid1>40 or (bcid1==0 and l1id1==0) ) C_PROB++;
         
      bool isBad=false;
      lastL1id = l1id2;
      if (lastL1id!=prevL1id){
	if (prevL1id!=123456) {
	  //cout << "   previous L1id: " << prevL1id << " has: " << nHit << " hists" << endl;
          if(nHit == 0 && false){
            cout << "########################### Zero hit event: " << endl;
            for(auto maltaword: event) cout << std::bitset<32>(maltaword.first) << " | " << std::bitset<32>(maltaword.second) << endl;
            cout << endl << endl;
            //event.clear();
            //m_cont = false;
	    //Stop();
          }
	  m_Hsize->Fill(nHit);
	  if ( lastL1id!=(prevL1id+1) ) {
	    //cout << " I am missing smoe L1ID: " << lastL1id << " != " << (prevL1id+1) << endl;
	    if ( lastL1id>prevL1id ) {
	      //cout << " .... adding: " << (lastL1id-prevL1id)-1 << " empty events!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
	      for (uint32_t c=0; c< (lastL1id-prevL1id)-1; c++) {
		m_Hsize->Fill(0.);
	      }
	    } else if (prevL1id>4000) {
	      int newL1=lastL1id+4096;
	      //cout << " .... adding Special: " <<  (newL1-prevL1id)-1 << " empty events!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
	      for (uint32_t c=0; c< (newL1-prevL1id)-1; c++)  m_Hsize->Fill(0.);
	    }
	  }
	}
	nHit=0;
	event.clear();
        if (lastL1id<prevL1id and prevL1id!=123456){
          if (prevL1id>lastL1id and (prevL1id-lastL1id)<3000) {// VD : was 2000
            isBad=true;
            C_JUMP++;
	    cout << m_name << "VERY BAD L1A jumps!!!!" << endl;
          } else {
            cout << m_name << " :: I am going to increase LAST VALERIO: " << lastL1id << "  prev: " << prevL1id ;
            cout << endl;
            //cout << m_name << " :: " << md2.toString()  << endl;
            //cout << m_name << " :: " << md.toString()  << endl;
          }
          if (not isBad) overflow+=1;
        }
        if (not isBad) prevL1id=lastL1id;
        else           lastL1id=prevL1id;

      }
      
      
      /////////////////////////////////////////////////////////////////////////////////
      //duplication calculation .....
      if((bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 and bcid1==63)){
        if ((winid2-winid1)==1 || (winid2==0 and winid1==7 )){
          if (phase2==0 && (l1id1==l1id2)){
            if (phase1>5){
              markDuplicate = true;
            }else if(phase1>3){
              markDuplicate = true;
            }else{
              markDuplicate = false;
            }
          }else{
            markDuplicate = false;
          }  
        }else{
          markDuplicate = false;
        }
      }else{
        markDuplicate = false;
      }
      m_ntuple->SetIsDuplicate(markDuplicate);
      
      //if(markDuplicate) cout << "Found duplicate word: " << std::bitset<32>(words[i+0]) << " | " << std::bitset<32>(words[i+1]) << endl;
      
      if (not isBad) m_ntuple->Fill();
      /*
      if (m_pX->GetEntries()>20000) {
	m_pX->Reset();
	m_pY->Reset();
      }
      */

      nHit+=md.getNhits();
      std::pair<uint32_t,uint32_t> mword; mword.first = words[i+0]; mword.second = words[i+1]; event.push_back(mword);
      if (not markDuplicate) {
	m_Htime->Fill(bcid2*25+winid2*3.125);
	for (unsigned int h=0; h<md2.getNhits(); ++h) {
	  //m_pX->Fill( ((float)md2.getHitRow(h)-256.)   *0.0368);
	  //m_pY->Fill( ((float)md2.getHitColumn(h)-256.)*0.0368);
	  m_Hmap->Fill(md2.getHitColumn(h),md2.getHitRow(h));
	}
      }
    }
    
    if(m_debug) cout << m_name << " After processing m_cont=" << m_cont << endl; 
    
    //if (countV%3==0) m_malta2->ReadMonitorWord(wordsDebug);
  }

  if(md.getWord1()!=0){
    m_ntuple->Set(&md);
    m_ntuple->SetIsDuplicate(false);
    m_ntuple->Fill();       
  }
}


uint32_t Malta2Module::GetNTriggers(){ return  m_extL1id; }

void Malta2Module::EnableFastSignal(){ m_malta2->EnableFastSignal(); }

void Malta2Module::DisableFastSignal(){ m_malta2->DisableFastSignal(); }

