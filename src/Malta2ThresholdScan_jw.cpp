//! -*-C++-*-

// MALTA2
#include "Malta2/Malta2Data.h"
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2ThresholdAnalysis.h"
#include "Malta2/Malta2Utils.h"

// For system calls
#include <cmdl/cmdargs.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>


using namespace std;

bool g_cont=true;
long int Scantime=1;//dummy
float Scantime_pix[512*512];//dummy
int rescan_first_point = 0;
int rescan_first_point_max = 3;
int target =0;
int m_thX = 1;
int m_thY= 10;
vector<int> *m_measured_points = NULL;
vector<int> *m_measured_triggers = NULL;
bool stop_scan = false;
bool find_middle = false;
TTree *m_config_tree;
std::vector<int>  config_value;
std::vector<string>  config_name;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int eval_new_point(vector<int> *v_point_to_scan, vector<int> *v_counters)
{
  int next_point = 0;
  if (v_counters->size()==0) next_point = v_point_to_scan->at(0);
  if (v_counters->size()==1) 
    {
      // CHECK THE PIXEL IS NOT DEAD, RE DO THE SCAN WITH ANOTHER INITIAL POINT
      if (v_counters->at(0)==0)
	{
	  ++rescan_first_point;
	  next_point = v_point_to_scan->at(0)+rescan_first_point;
	  v_counters->pop_back();
	}
      else next_point = v_point_to_scan->at(1);
    }
  if (v_counters->size() ==2) next_point = (int) (v_point_to_scan->at(0)+v_point_to_scan->at(1)) * 0.5 ;
  
  return next_point;
}

void eval_new_interval(vector<int> *v_point_to_scan, vector<int> *v_counters, int point, int counts)
{
  if (counts < target)
    {
      v_counters->at(1) = counts;
      v_point_to_scan->at(1) = point;
    }
  else
    {
      v_counters->at(0) = counts;
      v_point_to_scan->at(0) = point;       
    }
  
  // PROTECTION IF YOU ARE below Y/X thresholds
  if ( abs(v_point_to_scan->at(0)-v_point_to_scan->at(1))<= m_thX) find_middle = true;
  if ( abs(v_counters->at(0)-v_counters->at(1))<= m_thY) find_middle = true;
  
  // PROTECTION IF YOU ARE above/below middle point on Y
  if ( (abs(v_counters->at(0)) > target ) && (abs(v_counters->at(1)) > target ) ) stop_scan = true;
  if ( (abs(v_counters->at(0)) < target ) && (abs(v_counters->at(1)) < target ) ) stop_scan = true;
}

int find_middle_element(std::vector<int> *v)
{
  int element = 0;
  double diff = 999999999999;
  for (unsigned int i =0 ; i < v->size(); ++i)
    {
      if (abs(v->at(i)-target)< diff)
	{
	  element = i;
	  diff = abs(v->at(i)-target);
	}
    } 

  return element;
}


int main(int argc, char *argv[])
{
  //time
  time_t start,end;
  time_t start_pix,end_pix;
  float Scantime_pix[512*512];

  //Switch on Power Supply and set current & voltage limits (once).
  // systemcall("SetVoltage_VPulse.py -f -s -v 1.0");
  
  cout << "##########################################" << endl
       << "# Welcome to the MALTA2 Threshold Scan   #" << endl
       << "##########################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress( 'a',"address","address","ipbus address. Default 192.168.0.1");
  CmdArgStr     cOutdir(  'o',"output","output","output directory");
  CmdArgStr     cPath(    'f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgInt     cParamMin('l',"paramMin","paramMin","Vlow value(5-127)",CmdArg::isREQ);
  CmdArgInt     cParamMax('h',"paramMax","paramMax","Vhigh value(5-127)",CmdArg::isREQ);
  CmdArgInt     cParamStp('s',"paramStp","paramStp","Step of Voltage scan(1-127)",CmdArg::isREQ);
  CmdArgIntList cRegion(  'r',"region","region","region of pixels [xmin(0-511) No.x ymin(0-61) No.y(<62)]");
  CmdArgInt     cTrigger( 't',"trigger","trigger","the Number of pulse)",CmdArg::isREQ);
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgBool    cDut( 'd',"dut","is DUT");
  CmdArgBool    cFastScan( 'F',"fast scan","fast scan");
  CmdArgBool    cOnlyEvenPixel( 'e',"even pixel only","even pixel only");
  CmdArgStr     cConfig(  'c',"config","config","configuration file");
  CmdArgInt     cNthPix('n',"skip_n_pix","skip_n_pix","Scan every nth pixel",CmdArg::isREQ);
  CmdLine cmdl(*argv,&cVerbose,&cOutdir,&cDut,&cPath,&cAddress,&cRegion,&cParamMin,&cParamMax,&cParamStp,&cTrigger,&cQuiet,&cFastScan,&cConfig,&cOnlyEvenPixel,&cNthPix,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  char s[100]; 
  std::string cwd = getcwd(s, 100);
  TString workingDirectory(s);
  if (workingDirectory.EndsWith("MaltaSW/Malta2")==false){
    cout << "wrong working directory, plese run here: /home/sbmuser/MaltaSW/Malta2 " << endl;
    return 0;  
  } 
  //  string outdir = (cOutdir.flags() & CmdArg::GIVEN?cOutdir:"");
  string address = (cAddress.flags() & CmdArg::GIVEN?cAddress:"udp://ep-ade-gw-05.cern.ch:50003");
  vector<uint32_t> cPixels;//List of Pixels odd->x even->y
  vector<uint32_t> selPixX,selPixY;

  int nthPix = (int)cNthPix;
  
  if ( cRegion.flags() & CmdArg::GIVEN ) 
    {
      for(int32_t i=cRegion[0]; i<(cRegion[0]+cRegion[1]); i++)
	{
	  for(int32_t j=cRegion[2]; j<(cRegion[2]+cRegion[3]); j++)
	    {
              if ( cNthPix.flags() & CmdArg::GIVEN ){
                if(i%nthPix == 0 && j%nthPix == 0){
                  cPixels.push_back(i);
	          cPixels.push_back(j);
                }
              } else {
                cPixels.push_back(i);
	        cPixels.push_back(j);
              }
	    }
	}
    } 
  else 
    {
      cout << endl << " specify the region please " << endl << endl;
      exit(-1);
    }
  
  //for(uint32_t i=0; i<cPixels.count(); i++){
  for(uint32_t i=0; i<cPixels.size(); i++)
    {
      //std::cout << " val: " << cPixels[i] << std::endl;
      if(i%2==0)
	{
	  selPixX.push_back(cPixels[i]);
	}
      else
	{
	  selPixY.push_back(cPixels[i]);
	}
    }
  
  //// ANDREA maybe move this part in the .h, but there is not .h
  int maxY = (int) cTrigger*1.2; //TO BE FIXED be careful about the +20% due to some mismatch input and output
  target = (int)maxY*0.5;

  rescan_first_point = 0;

  vector<int> m_counters;
  vector<int> m_point_to_scan;
   
  //making output directory
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir="Results_ThScan/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");
  if (outdir!="") {
    Malta2Utils::SystemCall("mkdir -p "+outdir);
    Malta2Utils::SystemCall("mkdir -p "+outdir+"/hists");
  }
  
  bool noFile=( (cQuiet.flags() & CmdArg::GIVEN) ? cQuiet:false);  
  bool fastScan=( (cFastScan.flags() & CmdArg::GIVEN) ? cFastScan:false);
  bool onlyEvenPixel=( (cOnlyEvenPixel.flags() & CmdArg::GIVEN) ? cOnlyEvenPixel:false);
  
  cout << "Fast is: " << fastScan << endl;
  // exit(-1);

  signal(SIGINT,handler);
  
  //loop param
  int paramMin=cParamMin;   //5
  int paramMax=cParamMax;   //5
  if (paramMax>=90) 
    {
      cout << endl;
      cout << "WARNING maxVLOW should never exceed VHIGH value currently set at 90 ... please lower your max VLOW and retry" << endl;
      cout << endl;
      exit(-1);
    }

  int paramStp=cParamStp;
  //new loop param
  m_point_to_scan.reserve(2);
  m_point_to_scan.push_back(cParamMin);
  m_point_to_scan.push_back(cParamMax);
  m_measured_points = new vector<int>;
  m_measured_triggers = new vector<int>;
  //Trigger param
  int trigger=cTrigger;
  
  //Connect to MALTA2  
  Malta2 * malta = new Malta2(); 
  ipbus::Uhal* m_ipb = new ipbus::Uhal(address);
  malta->SetIPbus(m_ipb); // 1 ipbus call

  Malta2ThresholdAnalysis * ana = new Malta2ThresholdAnalysis((paramMax-paramMin)/paramStp,paramMin,paramMax,outdir);
  ana->setNPulse(trigger);
  if(cVerbose) std::cout << "after creating analysis" <<std::endl;

  //bool cConfig = true;
  malta->WriteConstDelays(4,1);
  
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);//was35
  //cConfig=false;

  cout << "#####################################" << endl
       << "# Configuring chip                  #" << endl
       << "#####################################" << endl;
  
  malta->SetVerbose(false);
  Malta2Utils::PreConfigureMalta(malta);
  ifstream fr;
  fr.open(string(cConfig).c_str());
  if (!fr.is_open()){
    cout << "#####################################################"<<endl;
    cout <<      "##  The configuration file cannot be opened: "+string(cConfig)+"        ##" << endl;
    cout << "Stopping script"<<endl;
    exit(0);
  }
  else{
    std::string str;
    //std::vector<int>  config_value;
    //std::vector<string>  config_name;
    while (std::getline(fr, str)) {
      std::string col1, col3;
      int col2;
      std::istringstream ss(str);
      ss >> col1 >> col2;
      TString variable(col1);
      if (variable.BeginsWith("SC_")) {
        config_name.push_back(variable.ReplaceAll(":","").Data());
        config_value.push_back(col2);
      }
    }
    m_config_tree = new TTree("config", "config");
    for (unsigned int i =0 ; i < config_value.size(); ++i){
      cout << config_name.at(i) << endl;
      m_config_tree->Branch(config_name.at(i).c_str(), &config_value.at(i), (config_name.at(i)+"/I").c_str());
    }
    m_config_tree->Fill();
  }

  malta->SetConfigFromFile(string(cConfig)); 

  Malta2Utils::EnableAll(malta);
  Malta2Utils::MaltaSend(malta);

  cout << "#####################################" << endl
       << "# Start Mask loop                   #" << endl
       << "#####################################" << endl;
    
  //Pixel loop

  time(&start);//all pixel scanning clock start

  int flug_Ysize=0;
  auto selPixY_min=min_element(selPixY.begin(),selPixY.end());
  auto selPixY_max=max_element(selPixY.begin(),selPixY.end());
  printf("%d %d %d\n",*selPixY_min,*selPixY_max,(*selPixY_max)-(*selPixY_min)+1);

  malta->ReadoutOff();

  int N_pix=1;
  int scPix=0;

  // there are 52 ipbus calls above this loop : 
  // not so many, and they only happen once ...
  switch(N_pix)
    {
    case 1: 
      //cout << __LINE__ << "\tMLB\t" << malta->GetNumCalls() << std::endl;
      for(uint32_t pixX=0;pixX<512;pixX++)
	{
	  if(selPixX.size()>0)
	    {
	      bool found=false;
	      for(uint32_t i=0;i<selPixX.size();i++)
		{
		  if(selPixX[i]==pixX){found=true;}
		}
	      if(!found){continue;}
	      if (onlyEvenPixel==true && (pixX % 2 != 0)) continue;
	    }
	  for(uint32_t pixY=0;pixY<512;pixY++)
	    {
	      if(selPixY.size()>0)
		{
		  bool found=false;
		  for(uint32_t i=0;i<selPixY.size();i++)
		    {
		      if(selPixY[i]==pixY){found=true;}
		    }
		  if(!found) continue;
		  if (onlyEvenPixel==true && (pixY % 2 != 0)) continue;
		}
	      
	      if(!g_cont) break;
	      time(&start_pix);//1pixel scanning clock start
	      if (rescan_first_point>0 && rescan_first_point_max)
		{
		  if(cConfig.flags()&&CmdArg::GIVEN)
		    {
		      if(cVerbose) cout << " trying to reconfigure the chip "  << endl;
		      //Malta2Utils::PreConfigureMalta(malta);
		    }
		}
	      rescan_first_point = 0;
	      scPix++;
	      if (scPix%100==0) cout << "Scanned: " << scPix << " pixels " << endl;
	      //Unmask desired pixel
	      if(cVerbose) cout << "Unmasking the desired pixel: " << pixX << "," << pixY << endl;
	      for (unsigned int p=0; p<512; p++) {
		malta->SetPixelPulseRow(p,false);
		malta->SetPixelPulseColumn(p,false);
		malta->SetPixelMaskRow(p,false);
		malta->SetPixelMaskColumn(p,false);
		malta->SetPixelMaskDiag(p,false);
	      } 
	      for (uint32_t a=0;a<256;a++) malta->SetDoubleColumnMask( a, false);
	      
	      malta->SetPixelPulseRow(pixY,true);
	      malta->SetPixelPulseColumn(pixX,true);
	      malta->SetDoubleColumnMask( (int)(pixX/2), true);	      
	      malta->SetPixelMaskRow(pixY, true);	      
	      
	      int newpixX=pixX;
              //div_t divresult = div (newpixX,16);
              //if ( divresult.rem <= 7 ) newpixX+=8;
              //if (newpixX<128) newpixX+=128;
              //if (newpixX>255 and newpixX<392) newpixX+=128;
	      //Parameter loop
	      float paramVal=paramMin;
	
	      //cout << "==> Start parameter loop" << endl;
	      //cout << "==> Pixel x: "<< pixX << " y: " << pixY << endl;
	    
	      stop_scan= false;
	      find_middle= false;
	      m_point_to_scan.at(0)= cParamMin;
	      m_point_to_scan.at(1)= cParamMax;
	      m_measured_points->clear();
	      m_measured_triggers->clear();
	      if (m_counters.size() > 0 ) m_counters.clear();
	      while (stop_scan==false && paramVal<=paramMax)
		{
		  //cout << __LINE__ << "\tMLB\t" << malta->GetNumCalls() << std::endl;
		  if (fastScan && find_middle==false) paramVal = eval_new_point(&m_point_to_scan, &m_counters);
		  else if  (fastScan && find_middle==true) 
		    {
		      stop_scan = true;
		      std::vector<int>::iterator it = std::lower_bound(m_measured_points->begin(), 
								       m_measured_points->end(),
								       m_measured_points->at(find_middle_element(m_measured_triggers)) +1 );

		      if (it != m_measured_points->end()) continue;
		      else paramVal = m_measured_points->at(find_middle_element(m_measured_triggers)) +1; 
		    }
		  m_measured_points->push_back(paramVal);
		  if(cVerbose) cout << "Vlow:\t" << paramVal << " / " << paramMax << endl;

		  if (rescan_first_point>=rescan_first_point_max)
		    {
		      cout << "The pixel's dead." << endl;
		      break;
		    }
		  ostringstream os;
		  
		  if(cVerbose) cout << "--------------- Setting vlow to: " << paramVal << " ---------------------" << endl;
		  malta->Write(malta->Word("SC_VPULSE_LOW"),paramVal,false);
		  Malta2Utils::MaltaSend(malta); // 7 ipbus calls
		  
		  if(!g_cont) break;
		  
		  //Create ntuple
		  os.str("");
		  os << outdir  << "/thrscan_"
		     << setw(3) << setfill('0') << pixX << "_"
		     << setw(3) << setfill('0') << pixY << "_"
		     << setw(3) << setfill('0') << paramVal << ".root";
		  string fname = os.str();
		  
		  if(cVerbose) cout << fname << endl;
		  Malta2Tree *ntuple = new Malta2Tree();
		  if (!noFile) ntuple->Open(fname,"RECREATE");
		  if(cVerbose) std::cout << "*************"+fname<<std::endl;
		  
		  //Flush MALTA
		  //malta->ReadoutOff();     // 2 ipbus calls
		  malta->ResetFifo();      // 2 ipbus calls
		  //malta->ResetL1Counter(); // 3 ipbus calls
		  
		  uint32_t event=0;
		  malta->ReadoutOn(); // 2 ipbus calls
		  //cout << __LINE__ << "\tMLB\t" << malta->GetNumCalls() << std::endl;
		  
		  // MLB added this magical /5
		  malta->Trigger(trigger,true); // 101 ipbus calls (Number of triggers + 1)
		  
		  //cout << __LINE__ << "\tMLB\t" << malta->GetNumCalls() << std::endl;
		  malta->ReadoutOff(); // 2 ipbus calls
		  
		  Malta2Data md,md2;
		  uint32_t words[250];
		  uint32_t numwords=250;
		  uint32_t numwordsCurrent=0;
		  for (uint32_t i=0;i<numwords;i+=2) {
		    words[i+0]=0;
		    words[i+1]=0;
		  }

		  bool isFirst=true;	  
		  //chrono::time_point<chrono::system_clock> t0 = chrono::system_clock::now();
		  int numberOfHitsInMyPixel=0;       
		  int numberOfHits=0;       

		  //cout << "There are " << numwordsCurrent << " words in the FIFO." << endl;

		  while(g_cont)
		    {	      
		      // MLB porting
		      numwordsCurrent = malta->GetFIFO2WordCount(); // 1 ipbus calls
		      if(numwordsCurrent >= 2) numwordsCurrent -= 2;
		      if(numwordsCurrent > numwords) 
			{
			  //cout << "numwordsCurrent > numwords !!!" << endl;
			  numwordsCurrent = numwords;
			}

		      malta->ReadMaltaWord(words,numwords); // 1 ipbus calls
		      
		      if (words[0]==0) break; // This is the way out of the loop.
		      
		      for (uint32_t i = 0; i < numwords; i+=2)
			{
		      
			  if (isFirst)
			    {
			      md.setWord1(words[0]);
			      md.setWord2(words[1]);
			      isFirst = false;
			      continue;
			    }
			  			  
			  md2.setWord1(md.getWord1());
			  md2.setWord2(md.getWord2());
			  md.setWord1(words[0+i]);
			  md.setWord2(words[1+i]);
			  md.unPack();
			  md2.unPack();
		      
			  //from MD (VD: this could be moved later to speed up)
			  for (unsigned h=0; h<md.getNhits(); h++) 
			    {
			      uint32_t LpixX=md.getHitColumn(h);
			      uint32_t LpixY=md.getHitRow(h);
			      numberOfHits++;
			      if(cVerbose) cout << LpixX << " / " << LpixY << endl;       
			      //if (LpixX==pixX and LpixY==pixY) numberOfHitsInMyPixel++;
			      if (LpixX==newpixX and LpixY==pixY) numberOfHitsInMyPixel++; // MLB got from VD 8Apr21
			    } 
		      
			  if (!noFile) ntuple->Set(&md2);
			  
			  uint32_t phase1 = md2.getPhase();
			  uint32_t winid1 = md2.getWinid();
			  uint32_t bcid1 = md2.getBcid();          
			  uint32_t l1id1 = md2.getL1id();          
			  
			  uint32_t phase2 = md.getPhase();
			  uint32_t winid2 = md.getWinid();
			  uint32_t bcid2 = md.getBcid();
			  uint32_t l1id2 = md.getL1id();	    
			  
			  // REWRITING DUPLICATION REOMVAL HERE		   		      
			  
			  bool markDuplicate    =   Malta2Utils::markDuplicate(bcid1,bcid2,winid1,winid2,l1id1,l1id2,phase1,phase2);
			  bool markDuplicateNext= Malta2Utils::isDuplicateNext(bcid1,bcid2,winid1,winid2,l1id1,l1id2,phase1,phase2);
			  
			  if (!noFile)
			    {
			      if( markDuplicate )
				{
				  ntuple->SetIsDuplicate(1);
				}
			      else 
				{
				  ntuple->SetIsDuplicate(0);
				}
			      ntuple->Fill();
			    }
			  
			  event+=1;
			  ana->process(&md2,pixX,newpixX,pixY,paramVal,markDuplicate);
			}
		    } // end while(g_cont){...}
		  
		  if(cVerbose) 
		    cout << "Number of events: " << event 
			 << " --> in the pixel I want to pulse: " 
			 << numberOfHitsInMyPixel << endl;
		  if(cVerbose) cout << "Total number of hits: " << numberOfHits<< endl;

		  if (fastScan)
		    {
		      if (m_counters.size()<2) m_counters.push_back(numberOfHitsInMyPixel);
		      if (m_counters.size()==2) eval_new_interval(&m_point_to_scan, 
								  &m_counters, 
								  paramVal, 
								  numberOfHitsInMyPixel); //check difference numberOfHitsInMyPixel/numberOfHits
		    }
		  if (numberOfHits ==0 ) ana->process(&md2,pixX,newpixX,pixY,paramVal,0);
		  
		  m_measured_triggers->push_back(numberOfHitsInMyPixel);
		  if (!noFile) ntuple->Close();
		  if (!fastScan)  paramVal+=paramStp;
		  //This line is needed to avoid filling the memory of the computer. Carlos
		  delete ntuple;
		} //end of VPULSE_L loop
	      
	      //End param loop
	  
	      // getting time information 
	      time(&end_pix);
	      ////ana->end(Scantime); //VD: we need to discuss about it
	      Scantime_pix[pixX*512+pixY]=end_pix-start_pix;
	      if(cVerbose) cout << "!!!!!!!one pixel scantime" << Scantime_pix[pixX*512+pixY] <<endl;
	    }
	}
      break;
      
    default:
      break;
    }
  
  ////////////////////////////////////////////////////////
  //m_config_tree->Scan();
  time(&end);
  float Scantime=end-start;
  cout << "TOTAL scantime: " << Scantime << endl;
  ana->end(0.,Scantime_pix, m_config_tree);
  
  delete malta;
  delete ana;


  
  if (onlyEvenPixel)
    { 
      //cout << ("python share/runThAnalysisMalta2.py -f "+outdir+" -e True").c_str() << endl;
      Malta2Utils::SystemCall(("python share/runThAnalysisMalta2.py -f "+outdir+" -e True").c_str());
    }
  else
    {
      //cout << ("python share/runThAnalysisMalta2.py -f "+outdir+" -e False").c_str() << endl;
      Malta2Utils::SystemCall(("python share/runThAnalysisMalta2.py -f "+outdir+" -e False").c_str());
    }

  cout << "LINE " << __LINE__ << "\tThere were " << malta->GetNumCalls() << " ipbus calls!" << std::endl;  
  cout << "Have a nice day!!" << endl;
  cout << "TOTAL scantime: " << Scantime << " sec for: " << scPix << "  pixels " << endl;
  return 0;
}
