/**
 * Malta2Utils contains common functions 
 * which are used to configure the
 * MALTA2 Pixel detector prototype.
 *
 * @brief MALTA2 common utilities
 * @author Andrea.Gabrielli@cern.ch
 * @author Matt.LeBlanc@cern.ch
 * @date April 2021
 **/

#include "Malta2/Malta2Utils.h"

void Malta2Utils::PreConfigureMalta(Malta2* malta, const int sleep1, const int sleep2)
{
  malta->Reset();
  //malta->WriteConstDelays(13,10);
  //malta->SetReadoutDelay(80);//was35

  //malta->SetVerbose(false);
  // set the voltages
  malta->SetDefaults(false);

  malta->Write(malta->Word("LVDS_VBCMFB"),0x7,false);

  malta->Write(malta->Word("SWCNTL_DACMONV"),1,false);
  malta->Write(malta->Word("SWCNTL_DACMONI"),1,false);

  malta->Write(malta->Word("SC_VCASN")      ,110,false);
  malta->Write(malta->Word("SC_VCLIP")      ,125,false);
  malta->Write(malta->Word("SC_VPULSE_HIGH"), 90,false);
  malta->Write(malta->Word("SC_VPULSE_LOW") , 10,false);
  malta->Write(malta->Word("SC_VRESET_P")   , 29,false);
  malta->Write(malta->Word("SC_VRESET_D")   , 65,false);
  malta->Write(malta->Word("SC_ICASN") , 10,false);
  malta->Write(malta->Word("SC_IRESET"), 30,false);
  malta->Write(malta->Word("SC_ITHR")  , 60,false);  //at 10 it works # #def is120
  malta->Write(malta->Word("SC_IBIAS") , 43,false);
  malta->Write(malta->Word("SC_IDB")   , 50,false);   //at 30 it works

  // mask everything
  //Malta2Utils::MaskAll(malta);
  //Malta2Utils::MaltaSend(malta, sleep1, sleep2);
}

void Malta2Utils::MaskAll(Malta2* malta)
{
  // n.b. that Malta2 masking currently has flipped logic!!

  for (int i =0; i < 256; ++i)
    {
      malta->SetDoubleColumnMask (i, true); 
    }
  for (int i =0; i < 512; ++i)
    {
      malta->SetPixelMaskColumn (i, true);
      malta->SetPixelMaskDiag   (i, true);
      malta->SetPixelMaskRow    (i, true);
    }
}

void Malta2Utils::EnableAll(Malta2* malta)
{
  // n.b. that Malta2 masking currently has flipped logic!!
  
  for (int i =0; i < 256; ++i)
    {
      malta->SetDoubleColumnMask (i, false);
    }
  for (int i =0; i < 512; ++i)
    {
      malta->SetPixelMaskColumn (i, false);
      malta->SetPixelMaskDiag   (i, false);
      malta->SetPixelMaskRow    (i, false);
    }
}

void Malta2Utils::MaltaSend(Malta2* malta, const int sleep1, const int sleep2)
{
  malta->Send();
  malta->SetState(0);
  malta->SetState(1);
}

stringstream Malta2Utils::SystemCall(std::string cmd)
{
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  cout << __LINE__ << endl;
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

bool Malta2Utils::isDuplicateNext(uint32_t bcid1,
				  uint32_t bcid2,
				  uint32_t winid1,
				  uint32_t winid2,
				  uint32_t l1id1,
				  uint32_t l1id2,
				  uint32_t phase1,
				  uint32_t phase2)
{
  bool markDuplicateNext=false;
  if( (bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 && bcid1==63))
    {
      if( (winid2-winid1)==1 || (winid2==0 && winid1==7 ))
	{
	  if( phase2==0 && (l1id1==l1id2))
	    {
	      if( phase1<5 and phase1>3)
		{
		  markDuplicateNext = true;
		}	      
	    }
	}
    }

  return markDuplicateNext;
}

bool Malta2Utils::markDuplicate(uint32_t bcid1,
				uint32_t bcid2,
				uint32_t winid1,
				uint32_t winid2,
				uint32_t l1id1,
				uint32_t l1id2,
				uint32_t phase1,
				uint32_t phase2)
{
  bool isDuplicate=false;
  if( (bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 && bcid1==63))
    {
      if( (winid2-winid1)==1 || (winid2==0 && winid1==7 ))
	{
	  if( phase2==0 && (l1id1==l1id2))
	    {
	      if( phase1>=5)
		{
		  isDuplicate = true;
		}
	    }
	}
    }

  return isDuplicate;
}
