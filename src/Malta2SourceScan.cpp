//! -*-C++-*-
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Data.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2Utils.h"
#include "TCanvas.h"
#include "TStyle.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <unistd.h>
#include <sys/stat.h>
#include <set>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"

using namespace std;
bool g_cont=true;

void handler(int) { 
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
    
  cout << "#####################################" << endl
       << "# Welcome to the new MALTA2 Source Scan #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgStr     cAddress( 'a',"address","address","connection string. upd://host:port",CmdArg::isREQ);
  CmdArgStr     cChip(    'c',"chip","sample","WXRX. Required",CmdArg::isREQ);
  CmdArgStr     cTag(     't',"tag","text","test description. Defatult test");
  CmdArgStr     cConfig(  'C',"config","config","configuration file");
  CmdArgInt     cTrigs(   'n',"ntrigs","number","number of triggers per DAQ window (2.5us). Default 10");
  CmdArgInt     cReps(    'N',"nreps","number","number of repetitions. Default 50");
  CmdArgStr     cDac(     'd',"dac","name","DAC to scan (IDB,ITHR). Default IDB");
  CmdArgInt     cParamMin('m',"paramMin","paramMin","Param low (5-127). Default 5");
  CmdArgInt     cParamMax('M',"paramMax","paramMax","Param high (5-127). Default 127");
  CmdArgInt     cParamStp('s',"paramStp","paramStp","Param step. Default 1");

  cTrigs = 10;
  cReps = 50;
  cParamMin=5;
  cParamMax=127;
  cParamStp=1;
  
  CmdLine cmdl(*argv,
	       &cVerbose,
	       &cQuiet,
	       &cAddress,
	       &cChip,
	       &cTag,
	       &cConfig,
	       &cTrigs,
	       &cReps,
	       &cDac,
	       &cParamMin,
	       &cParamMax,
	       &cParamStp,
	       NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  //find the actual DAC
  uint32_t dIDB=0;
  uint32_t dITHR=1;
  uint32_t iDac=dIDB;
  string sDac=(cDac.flags() & CmdArg::GIVEN?string(cDac):"IDB");
  if     (sDac=="IDB"){  iDac=dIDB;  cout << "Scanning IDB" << endl; } 
  else if(sDac=="ITHR"){ iDac=dITHR; cout << "Scanning ITHR" << endl; }
  else{ cout << "DAC not supported: " << sDac << endl; return 0; }

  //making output directory
  string outdir=getenv("MALTA_DATA_PATH");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Malta2";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Results_SourceScan";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+string(cChip);
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+(cTag.flags() & CmdArg::GIVEN?string(cTag):"test");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(!cQuiet) mkdir((outdir+"/hits").c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  
  //Connect to MALTA2
  Malta2 * malta = new Malta2();
  malta->Connect(string(cAddress));

  malta->SetVerbose(false);
  malta->WriteConstDelays(4,1);
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);
  
  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;

  Malta2Utils::PreConfigureMalta(malta);
  Malta2Utils::EnableAll(malta);
  if (cConfig.flags()){
    if(!malta->SetConfigFromFile(string(cConfig))){
      cout << "## Cannot parse config file: " << string(cConfig) << endl;
      cout << "## Exit"<<endl;
      delete malta;
      exit(0);
    }
  }
  else{
    cout << "##  using default config from Malta2Utils" << endl;
  }
  
  cout << "#####################################" << endl
       << "# Start Rate Loop                   #" << endl
       << "#####################################" << endl;

  time_t scan_start,scan_stop;
  time_t loop_start,loop_stop;
   
  float DAQwindow = 2500e-9;
  float totTime = cTrigs*DAQwindow*cReps;
  uint32_t numPixelsMasked = 0;
  
  vector<TH2I*> vnHits_h2;
  TGraphErrors * gHitsVsDAC = new TGraphErrors();
  gHitsVsDAC->SetNameTitle("hits_vs_DAC",";param [DAC]; Chip hits [entries]"); // sDac instead of "param"
  
  signal(SIGINT,handler);
  time(&scan_start);
  uint32_t nParamSteps=0;

  // loop over DAC value of IDB or ITH
  for(uint32_t paramVal=cParamMin;paramVal<=cParamMax;paramVal+=cParamStp){
  	cout << "Param: " << paramVal << endl;
    float hitNum = 0;
    float hitRate = 0;

  	//Change the DAC
    if(iDac==dIDB){
      malta->SetIDB(paramVal);
      cout << "IDB set to " << paramVal << endl;
      //if(!cQuiet) ntuple->SetIDB(paramVal);
    }
    else if(iDac==dITHR){
      malta->SetITHR(paramVal);
      cout << "ITH set to " << paramVal << endl;
      //if(!cQuiet) ntuple->SetITHR(paramVal);
    }
    if(!g_cont){break;}

    time(&loop_start);
    
    Malta2Utils::MaltaSend(malta);

    TH2I* nHits_h2 = new TH2I(("Hits_"+to_string(paramVal)).c_str(),"Occupancy;column;row;counts",512,-0.5,511.5,512,-0.5,511.5);
    vnHits_h2.push_back(nHits_h2);
    
    //Create ntuple
    Malta2Tree *ntuple = new Malta2Tree();
    if (!cQuiet){
      ostringstream os;
      os << outdir  << "/hits/sourcescan_" << setw(3) << setfill('0') << paramVal << ".root";
      ntuple->Open(os.str(),"RECREATE");
    }
    
    //repetitions
    for( int r=0; r<cReps; r++) {
      if(!g_cont){break;}
      
      //Flush MALTA
      malta->ResetFifo();
      malta->ResetL1Counter();
      malta->ReadoutOn();
      malta->Trigger(cTrigs,false);
      malta->ReadoutOff();

      Malta2Data md,md2;
      int maxwords=200;
      uint32_t words[200];
      for (uint32_t i=0;i<maxwords;i++){ words[i]=0;}
      
      bool isFirst=true;
      int numberOfHits=0;
      
      //Readout 
      while(g_cont){
        int numwords = malta->GetFIFO2WordCount();
        if(numwords > maxwords){ numwords = maxwords; }
        malta->ReadMaltaWord(words,numwords);

        // This is the way out of the loop.
        if (words[0]==0){ break; }
        for (uint32_t i = 0; i < numwords; i+=2){		      
          if (isFirst){
            md.setWord1(words[0]);
            md.setWord2(words[1]);
            isFirst = false;
            continue;
          }
          md2.setWord1(md.getWord1());
          md2.setWord2(md.getWord2());
          md.setWord1(words[0+i]);
          md.setWord2(words[1+i]);
          md.unPack();
          md2.unPack();

          bool markDuplicate = Malta2Utils::markDuplicate(md2.getBcid(),md.getBcid(),
  				md2.getWinid(),md.getWinid(),
          md2.getL1id(),md.getL1id(),
  				md2.getPhase(),md.getPhase());
          if (!cQuiet){
            ntuple->Set(&md2);
            ntuple->SetIsDuplicate(markDuplicate?1:0);
            ntuple->Fill();
          }
          if (markDuplicate==0 ){ 
            for (unsigned h=0; h<md2.getNhits(); h++){
              uint32_t pixX=md2.getHitColumn(h);
              uint32_t pixY=md2.getHitRow(h);
              if(cVerbose) cout << "hit: " << pixX << "," << pixY << endl;
              if(malta->IsPixelMasked(pixX,pixY)){
                if(cVerbose){cout << "Pixel should be masked: " << pixX << ", " << pixY 
                  << " Check if the threshold is too low and the hits are being merged" << endl;}
              }
              numberOfHits++;
              nHits_h2->Fill(pixX, pixY);
            }
          }
        }
      }
    cout << "Number of hits: " << numberOfHits << endl;
    if (numberOfHits > 500000) {
      cout << "The FIFO is full, aborting scan" << endl;
      g_cont=false;
      break; 
      }
    } //end of repetition loop

    time(&loop_stop);
    cout << "Elapsed time [s]: " << (loop_stop-loop_start) << endl;
    
    if (!cQuiet) ntuple->Close();
    delete ntuple;
    hitNum = (float)nHits_h2->GetEntries();
    hitRate = (float)nHits_h2->GetEntries()/totTime;

  	//Fill histograms
    gHitsVsDAC->SetPoint(nParamSteps,(float)paramVal,hitNum);
    gHitsVsDAC->SetPointError(nParamSteps,0,sqrt(hitNum));
    cout << "Hits : " << hitNum << endl;
    cout << "Hit rate: " << hitRate << " Hz" << endl;
    nParamSteps++;
  } // end of param loop
  
  time(&scan_stop);
  cout << "Scan time [s]: " << (scan_stop-scan_start) << endl;
  cout << "Number of Parameter steps: " << nParamSteps << endl;
  cout << "Cleaning the house" << endl;
  delete malta;

  cout << "Print out some plots" << endl;
  TCanvas *can = new TCanvas("can", "can", 800, 600);
  gHitsVsDAC->Draw("APL");
  can->Print((outdir+"/HitsVsDAC.pdf").c_str());
  can->SetLogy(1);
  can->Print((outdir+"/HitsVsDAC_logy.pdf").c_str());
  delete can;

  cout << "Write root file" << endl;
  TFile * tout = new TFile( (outdir+"/source_scan_results.root").c_str(), "RECREATE");
  tout->cd();
  gHitsVsDAC->Write();
  for(TH2I* h2: vnHits_h2){h2->Write();}
  tout->Close();
  delete tout;
  
  cout << "Have a nice day" << endl;
  return 0;
}
